// Copyright (c) 2016-2017, Qijie Chu.

use std::{str, fmt, hash};
use std::ops::{Add, Sub};

use prelude::*;
//use format::{Item, Numeric, Pad, Fixed};
//use format::{parse, Parsed, ParseError, ParseResult, DelayedFormat, StrftimeItems};

#[derive(PartialEq, Eq, PartialOrd, Ord, Copy, Clone)]
pub struct RawTime {
    secs: u32,
    frac: u32,
}

impl RawTime {
    /// Makes a new `RawTime` from the serialized representation.
    /// Used for serialization formats.
    #[inline]
    pub fn new(secs: u32, frac: u32) -> Option<RawTime> {
        // check if the values are in the range
        if secs >= 86400 || frac >= 1_000_000_000 { return None; }
        Some(RawTime { secs: secs, frac: frac })
    }

    #[cfg(feature = "rustc-serialize")]
    fn from_serialized(secs: u32, frac: u32) -> Option<RawTime> {
        RawTime::new(secs, frac)
    }

    /// Returns a serialized representation of this `RawDate`.
    #[cfg(feature = "rustc-serialize")]
    fn to_serialized(&self) -> (u32, u32) {
        (self.secs, self.frac)
    }

    /// Returns a triple of the hour, minute and second numbers.
    #[inline]
    pub fn hms(&self) -> (u32, u32, u32) {
        let (mins, sec) = div_mod_floor(self.secs, 60);
        let (hour, min) = div_mod_floor(mins, 60);
        (hour, min, sec)
    }

    #[inline]
    pub fn from_timestamp(secs: f64) -> Option<RawTime> {
        if secs >= 86400. { 
            return None; 
        }
        let secs_int = secs as u32;
        let nano = (secs - secs_int as f64) * 1_000_000_000.;

        Some(RawTime { secs: secs_int, frac: nano as u32 })
    }
    
    #[inline]
    pub fn from_hms(hour: u32, min: u32, sec: u32) -> Option<RawTime> {
        let _sec = hour * 3600 + min * 60 + sec;
        RawTime::from_timestamp(_sec as f64)
    }
    
    /// Makes a new `RawTime` from hour, minute, second and millisecond.
    /// The millisecond part can exceed 1,000
    /// in order to represent the [leap second](./index.html#leap-second-handling).
    /// Returns `None` on invalid hour, minute, second and/or millisecond.
    #[inline]
    pub fn from_hms_milli(hour: u32, min: u32, sec: u32, milli: u32) -> Option<RawTime> {
        milli.checked_mul(1_000_000)
             .and_then(|nano| RawTime::from_hms_nano(hour, min, sec, nano))
    }

    /// Makes a new `RawTime` from hour, minute, second and microsecond.
    /// The microsecond part can exceed 1,000,000
    /// in order to represent the [leap second](./index.html#leap-second-handling).
    /// Returns `None` on invalid hour, minute, second and/or microsecond.
    #[inline]
    pub fn from_hms_micro(hour: u32, min: u32, sec: u32, micro: u32) -> Option<RawTime> {
        micro.checked_mul(1_000)
             .and_then(|nano| RawTime::from_hms_nano(hour, min, sec, nano))
    }

    /// Makes a new `RawTime` from hour, minute, second and nanosecond.
    /// The nanosecond part can exceed 1,000,000,000
    /// in order to represent the [leap second](./index.html#leap-second-handling).
    /// Returns `None` on invalid hour, minute, second and/or nanosecond.
    #[inline]
    pub fn from_hms_nano(hour: u32, min: u32, sec: u32, nano: u32) -> Option<RawTime> {
        if hour >= 24 || min >= 60 || sec >= 60 || nano >= 1_000_000_000 {
            return None; 
        }
        let _sec = hour * 3600 + min * 60 + sec;
        Some(RawTime { secs: _sec, frac: nano })
    }

    /// Adds given `Duration` to the current time,
    /// and also returns the number of *seconds*
    /// in the integral number of days ignored from the addition.
    /// (We cannot return `Duration` because it is subject to overflow or underflow.)
    ///
    /// # Example
    ///
    /// ~~~~
    /// use chrono::{RawTime, Duration};
    ///
    /// let from_hms = RawTime::from_hms;
    ///
    /// assert_eq!(from_hms(3, 4, 5).overflowing_add(Duration::hours(11)),
    ///            (from_hms(14, 4, 5), 0));
    /// assert_eq!(from_hms(3, 4, 5).overflowing_add(Duration::hours(23)),
    ///            (from_hms(2, 4, 5), 86400));
    /// assert_eq!(from_hms(3, 4, 5).overflowing_add(Duration::hours(-7)),
    ///            (from_hms(20, 4, 5), -86400));
    /// ~~~~
    pub fn overflowing_add(&self, mut rhs: Duration) -> (RawTime, i64) {
        let mut secs = self.secs;
        let mut frac = self.frac;

        // check if `self` is a leap second and adding `rhs` would escape that leap second.
        // if it's the case, update `self` and `rhs` to involve no leap second;
        // otherwise the addition immediately finishes.
        if frac >= 1_000_000_000 {
            let rfrac = 2_000_000_000 - frac;
            if rhs >= Duration::nanoseconds(rfrac as i64) {
                rhs = rhs - Duration::nanoseconds(rfrac as i64);
                secs += 1;
                frac = 0;
            } else if rhs < Duration::nanoseconds(-(frac as i64)) {
                rhs = rhs + Duration::nanoseconds(frac as i64);
                frac = 0;
            } else {
                frac = (frac as i64 + rhs.num_nanoseconds().unwrap()) as u32;
                debug_assert!(frac < 2_000_000_000);
                return (RawTime { secs: secs, frac: frac }, 0);
            }
        }
        debug_assert!(secs <= 86400);
        debug_assert!(frac < 1_000_000_000);

        let rhssecs = rhs.num_seconds();
        let rhsfrac = (rhs - Duration::seconds(rhssecs)).num_nanoseconds().unwrap();
        debug_assert!(Duration::seconds(rhssecs) + Duration::nanoseconds(rhsfrac) == rhs);
        let rhssecsinday = rhssecs % 86400;
        let mut morerhssecs = rhssecs - rhssecsinday;
        let rhssecs = rhssecsinday as i32;
        let rhsfrac = rhsfrac as i32;
        debug_assert!(-86400 < rhssecs && rhssecs < 86400);
        debug_assert!(morerhssecs % 86400 == 0);
        debug_assert!(-1_000_000_000 < rhsfrac && rhsfrac < 1_000_000_000);

        let mut secs = secs as i32 + rhssecs;
        let mut frac = frac as i32 + rhsfrac;
        debug_assert!(-86400 < secs && secs < 2 * 86400);
        debug_assert!(-1_000_000_000 < frac && frac < 2_000_000_000);

        if frac < 0 {
            frac += 1_000_000_000;
            secs -= 1;
        } else if frac >= 1_000_000_000 {
            frac -= 1_000_000_000;
            secs += 1;
        }
        debug_assert!(-86400 <= secs && secs < 2 * 86400);
        debug_assert!(0 <= frac && frac < 1_000_000_000);

        if secs < 0 {
            secs += 86400;
            morerhssecs -= 86400;
        } else if secs >= 86400 {
            secs -= 86400;
            morerhssecs += 86400;
        }
        debug_assert!(0 <= secs && secs < 86400);

        (RawTime { secs: secs as u32, frac: frac as u32 }, morerhssecs)
    }

    /// Subtracts given `Duration` from the current time,
    /// and also returns the number of *seconds*
    /// in the integral number of days ignored from the subtraction.
    /// (We cannot return `Duration` because it is subject to overflow or underflow.)
    ///
    /// # Example
    ///
    /// ~~~~
    /// use chrono::{RawTime, Duration};
    ///
    /// let from_hms = RawTime::from_hms;
    ///
    /// assert_eq!(from_hms(3, 4, 5).overflowing_sub(Duration::hours(2)),
    ///            (from_hms(1, 4, 5), 0));
    /// assert_eq!(from_hms(3, 4, 5).overflowing_sub(Duration::hours(17)),
    ///            (from_hms(10, 4, 5), 86400));
    /// assert_eq!(from_hms(3, 4, 5).overflowing_sub(Duration::hours(-22)),
    ///            (from_hms(1, 4, 5), -86400));
    /// ~~~~
    #[inline]
    pub fn overflowing_sub(&self, rhs: Duration) -> (RawTime, i64) {
        let (time, rhs) = self.overflowing_add(-rhs);
        (time, -rhs) // safe to negate, rhs is within +/- (2^63 / 1000)
    }
    /*
    /// Parses a string with the specified format string and returns a new `RawTime`.
    /// See the [`format::strftime` module](../../format/strftime/index.html)
    /// on the supported escape sequences.
    ///
    /// # Example
    ///
    /// ~~~~
    /// use chrono::RawTime;
    ///
    /// let parse_from_str = RawTime::parse_from_str;
    ///
    /// assert_eq!(parse_from_str("23:56:04", "%H:%M:%S"),
    ///            Ok(RawTime::from_hms(23, 56, 4)));
    /// assert_eq!(parse_from_str("pm012345.6789", "%p%I%M%S%.f"),
    ///            Ok(RawTime::from_hms_micro(13, 23, 45, 678_900)));
    /// ~~~~
    ///
    /// Date and offset is ignored for the purpose of parsing.
    ///
    /// ~~~~
    /// # use chrono::RawTime;
    /// # let parse_from_str = RawTime::parse_from_str;
    /// assert_eq!(parse_from_str("2014-5-17T12:34:56+09:30", "%Y-%m-%dT%H:%M:%S%z"),
    ///            Ok(RawTime::from_hms(12, 34, 56)));
    /// ~~~~
    ///
    /// [Leap seconds](./index.html#leap-second-handling) are correctly handled by
    /// treating any time of the form `hh:mm:60` as a leap second.
    /// (This equally applies to the formatting, so the round trip is possible.)
    ///
    /// ~~~~
    /// # use chrono::RawTime;
    /// # let parse_from_str = RawTime::parse_from_str;
    /// assert_eq!(parse_from_str("08:59:60.123", "%H:%M:%S%.f"),
    ///            Ok(RawTime::from_hms_milli(8, 59, 59, 1_123)));
    /// ~~~~
    ///
    /// Missing seconds are assumed to be zero,
    /// but out-of-bound times or insufficient fields are errors otherwise.
    ///
    /// ~~~~
    /// # use chrono::RawTime;
    /// # let parse_from_str = RawTime::parse_from_str;
    /// assert_eq!(parse_from_str("7:15", "%H:%M"),
    ///            Ok(RawTime::from_hms(7, 15, 0)));
    ///
    /// assert!(parse_from_str("04m33s", "%Mm%Ss").is_err());
    /// assert!(parse_from_str("12", "%H").is_err());
    /// assert!(parse_from_str("17:60", "%H:%M").is_err());
    /// assert!(parse_from_str("24:00:00", "%H:%M:%S").is_err());
    /// ~~~~
    ///
    /// All parsed fields should be consistent to each other, otherwise it's an error.
    /// Here `%H` is for 24-hour clocks, unlike `%I`,
    /// and thus can be independently determined without AM/PM.
    ///
    /// ~~~~
    /// # use chrono::RawTime;
    /// # let parse_from_str = RawTime::parse_from_str;
    /// assert!(parse_from_str("13:07 AM", "%H:%M %p").is_err());
    /// ~~~~
    
    pub fn parse_from_str(s: &str, fmt: &str) -> ParseResult<RawTime> {
        let mut parsed = Parsed::new();
        try!(parse(&mut parsed, s, StrftimeItems::new(fmt)));
        parsed.to_raw_time()
    }

    /// Formats the time with the specified formatting items.
    /// Otherwise it is same to the ordinary [`format`](#method.format) method.
    ///
    /// The `Iterator` of items should be `Clone`able,
    /// since the resulting `DelayedFormat` value may be formatted multiple times.
    ///
    /// # Example
    ///
    /// ~~~~
    /// use chrono::RawTime;
    /// use chrono::format::strftime::StrftimeItems;
    ///
    /// let fmt = StrftimeItems::new("%H:%M:%S");
    /// let t = RawTime::from_hms(23, 56, 4);
    /// assert_eq!(t.format_with_items(fmt.clone()).to_string(), "23:56:04");
    /// assert_eq!(t.format("%H:%M:%S").to_string(),             "23:56:04");
    /// ~~~~
    ///
    /// The resulting `DelayedFormat` can be formatted directly via the `Display` trait.
    ///
    /// ~~~~
    /// # use chrono::RawTime;
    /// # use chrono::format::strftime::StrftimeItems;
    /// # let fmt = StrftimeItems::new("%H:%M:%S").clone();
    /// # let t = RawTime::from_hms(23, 56, 4);
    /// assert_eq!(format!("{}", t.format_with_items(fmt)), "23:56:04");
    /// ~~~~
    #[inline]
    pub fn format_with_items<'a, I>(&self, items: I) -> DelayedFormat<I>
            where I: Iterator<Item=Item<'a>> + Clone {
        DelayedFormat::new(None, Some(self.clone()), items)
    }

    /// Formats the time with the specified format string.
    /// See the [`format::strftime` module](../../format/strftime/index.html)
    /// on the supported escape sequences.
    ///
    /// This returns a `DelayedFormat`,
    /// which gets converted to a string only when actual formatting happens.
    /// You may use the `to_string` method to get a `String`,
    /// or just feed it into `print!` and other formatting macros.
    /// (In this way it avoids the redundant memory allocation.)
    ///
    /// A wrong format string does *not* issue an error immediately.
    /// Rather, converting or formatting the `DelayedFormat` fails.
    /// You are recommended to immediately use `DelayedFormat` for this reason.
    ///
    /// # Example
    ///
    /// ~~~~
    /// use chrono::RawTime;
    ///
    /// let t = RawTime::from_hms_nano(23, 56, 4, 12_345_678);
    /// assert_eq!(t.format("%H:%M:%S").to_string(), "23:56:04");
    /// assert_eq!(t.format("%H:%M:%S%.6f").to_string(), "23:56:04.012345");
    /// assert_eq!(t.format("%-I:%M %p").to_string(), "11:56 PM");
    /// ~~~~
    ///
    /// The resulting `DelayedFormat` can be formatted directly via the `Display` trait.
    ///
    /// ~~~~
    /// # use chrono::RawTime;
    /// # let t = RawTime::from_hms_nano(23, 56, 4, 12_345_678);
    /// assert_eq!(format!("{}", t.format("%H:%M:%S")), "23:56:04");
    /// assert_eq!(format!("{}", t.format("%H:%M:%S%.6f")), "23:56:04.012345");
    /// assert_eq!(format!("{}", t.format("%-I:%M %p")), "11:56 PM");
    /// ~~~~
    #[inline]
    pub fn format<'a>(&self, fmt: &'a str) -> DelayedFormat<StrftimeItems<'a>> {
        self.format_with_items(StrftimeItems::new(fmt))
    }

    
    */
}

impl Timelike for RawTime {
    #[inline]
    fn hour(&self) -> u32 {
        self.hms().0
    }

    #[inline]
    fn minute(&self) -> u32 {
        self.hms().1
    }

    #[inline]
    fn second(&self) -> u32 {
        self.hms().2
    }

    #[inline]
    fn nanosecond(&self) -> u32 {
        self.frac
    }

    fn timestamp(&self) -> f64 {
        self.secs as f64 + self.frac as f64 / 1_000_000_000.
    }

    #[inline]
    fn with_hour(&self, hour: u32) -> Option<RawTime> {
        if hour >= 24 { return None; }
        let secs = hour * 3600 + self.secs % 3600;
        Some(RawTime { secs: secs, ..*self })
    }

    #[inline]
    fn with_minute(&self, min: u32) -> Option<RawTime> {
        if min >= 60 { return None; }
        let secs = self.secs / 3600 * 3600 + min * 60 + self.secs % 60;
        Some(RawTime { secs: secs, ..*self })
    }

    #[inline]
    fn with_second(&self, sec: u32) -> Option<RawTime> {
        if sec >= 60 { return None; }
        let secs = self.secs / 60 * 60 + sec;
        Some(RawTime { secs: secs, ..*self })
    }

    #[inline]
    fn with_nanosecond(&self, nano: u32) -> Option<RawTime> {
        if nano >= 2_000_000_000 { return None; }
        Some(RawTime { frac: nano, ..*self })
    }
    
    #[inline]
    fn into_sec(&self) -> u32 {
        self.secs
    }
}

// `RawTime` can be used as a key to the hash maps (in principle).
// Practically this also takes account of fractional seconds, so it is not recommended.
// (For the obvious reason this also distinguishes leap seconds from non-leap seconds.)
impl hash::Hash for RawTime {
    fn hash<H: hash::Hasher>(&self, state: &mut H) {
        self.secs.hash(state);
        self.frac.hash(state);
    }
}

impl Add<Duration> for RawTime {
    type Output = RawTime;

    #[inline]
    fn add(self, rhs: Duration) -> RawTime {
        self.overflowing_add(rhs).0
    }
}

impl Sub<RawTime> for RawTime {
    type Output = Duration;

    fn sub(self, rhs: RawTime) -> Duration {
        //     |    |    :leap|    |    |    |    |    |    |    :leap|    |
        //     |    |    :    |    |    |    |    |    |    |    :    |    |
        // ----+----+-----*---+----+----+----+----+----+----+-------*-+----+----
        //          |   `rhs` |                             |    `self`
        //          |======================================>|       |
        //          |     |  `self.secs - rhs.secs`         |`self.frac`
        //          |====>|   |                             |======>|
        //      `rhs.frac`|========================================>|
        //          |     |   |        `self - rhs`         |       |

        use std::cmp::Ordering;

        let secs = self.secs as i64 - rhs.secs as i64;
        let frac = self.frac as i64 - rhs.frac as i64;

        // `secs` may contain a leap second yet to be counted
        let adjust = match self.secs.cmp(&rhs.secs) {
            Ordering::Greater => if rhs.frac >= 1_000_000_000 { 1 } else { 0 },
            Ordering::Equal => 0,
            Ordering::Less => if self.frac >= 1_000_000_000 { -1 } else { 0 },
        };

        Duration::seconds(secs + adjust) + Duration::nanoseconds(frac)
    }
}

impl Sub<Duration> for RawTime {
    type Output = RawTime;

    #[inline]
    fn sub(self, rhs: Duration) -> RawTime {
        self.overflowing_sub(rhs).0
    }
}

/// The `Debug` output of the raw time `t` is same to
/// [`t.format("%H:%M:%S%.f")`](../../format/strftime/index.html).
///
/// The string printed can be readily parsed via the `parse` method on `str`.
///
/// It should be noted that, for leap seconds not on the minute boundary,
/// it may print a representation not distinguishable from non-leap seconds.
/// This doesn't matter in practice, since such leap seconds never happened.
/// (By the time of the first leap second on 1972-06-30,
/// every time zone offset around the world has standardized to the 5-minute alignment.)
///
/// # Example
///
/// ~~~~
/// use chrono::RawTime;
///
/// assert_eq!(format!("{:?}", RawTime::from_hms(23, 56, 4)),              "23:56:04");
/// assert_eq!(format!("{:?}", RawTime::from_hms_milli(23, 56, 4, 12)),    "23:56:04.012");
/// assert_eq!(format!("{:?}", RawTime::from_hms_micro(23, 56, 4, 1234)),  "23:56:04.001234");
/// assert_eq!(format!("{:?}", RawTime::from_hms_nano(23, 56, 4, 123456)), "23:56:04.000123456");
/// ~~~~
///
/// Leap seconds may also be used.
///
/// ~~~~
/// # use chrono::RawTime;
/// assert_eq!(format!("{:?}", RawTime::from_hms_milli(6, 59, 59, 1_500)), "06:59:60.500");
/// ~~~~
impl fmt::Debug for RawTime {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let (hour, min, sec) = self.hms();
        let (sec, nano) = if self.frac >= 1_000_000_000 {
            (sec + 1, self.frac - 1_000_000_000)
        } else {
            (sec, self.frac)
        };

        try!(write!(f, "{:02}:{:02}:{:02}", hour, min, sec));
        if nano == 0 {
            Ok(())
        } else if nano % 1_000_000 == 0 {
            write!(f, ".{:03}", nano / 1_000_000)
        } else if nano % 1_000 == 0 {
            write!(f, ".{:06}", nano / 1_000)
        } else {
            write!(f, ".{:09}", nano)
        }
    }
}

/// The `Display` output of the raw time `t` is same to
/// [`t.format("%H:%M:%S%.f")`](../../format/strftime/index.html).
///
/// The string printed can be readily parsed via the `parse` method on `str`.
///
/// It should be noted that, for leap seconds not on the minute boundary,
/// it may print a representation not distinguishable from non-leap seconds.
/// This doesn't matter in practice, since such leap seconds never happened.
/// (By the time of the first leap second on 1972-06-30,
/// every time zone offset around the world has standardized to the 5-minute alignment.)
///
/// # Example
///
/// ~~~~
/// use chrono::RawTime;
///
/// assert_eq!(format!("{}", RawTime::from_hms(23, 56, 4)),              "23:56:04");
/// assert_eq!(format!("{}", RawTime::from_hms_milli(23, 56, 4, 12)),    "23:56:04.012");
/// assert_eq!(format!("{}", RawTime::from_hms_micro(23, 56, 4, 1234)),  "23:56:04.001234");
/// assert_eq!(format!("{}", RawTime::from_hms_nano(23, 56, 4, 123456)), "23:56:04.000123456");
/// ~~~~
///
/// Leap seconds may also be used.
///
/// ~~~~
/// # use chrono::RawTime;
/// assert_eq!(format!("{}", RawTime::from_hms_milli(6, 59, 59, 1_500)), "06:59:60.500");
/// ~~~~
impl fmt::Display for RawTime {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { fmt::Debug::fmt(self, f) }
}

/// Parsing a `str` into a `RawTime` uses the same format,
/// [`%H:%M:%S%.f`](../../format/strftime/index.html), as in `Debug` and `Display`.
///
/// # Example
///
/// ~~~~
/// use chrono::RawTime;
///
/// let t = RawTime::from_hms(23, 56, 4);
/// assert_eq!("23:56:04".parse::<RawTime>(), Ok(t));
///
/// let t = RawTime::from_hms_nano(23, 56, 4, 12_345_678);
/// assert_eq!("23:56:4.012345678".parse::<RawTime>(), Ok(t));
///
/// let t = RawTime::from_hms_nano(23, 59, 59, 1_234_567_890); // leap second
/// assert_eq!("23:59:60.23456789".parse::<RawTime>(), Ok(t));
///
/// assert!("foo".parse::<RawTime>().is_err());
/// ~~~~
/*
impl str::FromStr for RawTime {
    type Err = ParseError;

    fn from_str(s: &str) -> ParseResult<RawTime> {
        const ITEMS: &'static [Item<'static>] = &[
            Item::Space(""), Item::Numeric(Numeric::Hour, Pad::Zero),
            Item::Space(""), Item::Literal(":"),
            Item::Space(""), Item::Numeric(Numeric::Minute, Pad::Zero),
            Item::Space(""), Item::Literal(":"),
            Item::Space(""), Item::Numeric(Numeric::Second, Pad::Zero),
            Item::Fixed(Fixed::Nanosecond), Item::Space(""),
        ];

        let mut parsed = Parsed::new();
        try!(parse(&mut parsed, s, ITEMS.iter().cloned()));
        parsed.to_raw_time()
    }
}
*/
#[cfg(feature = "rustc-serialize")]
mod rustc_serialize {
    use super::RawTime;
    use rustc_serialize::{Encodable, Encoder, Decodable, Decoder};

    // TODO the current serialization format is NEVER intentionally defined.
    // this basically follows the automatically generated implementation for those traits,
    // plus manual verification steps for avoiding security problem.
    // in the future it is likely to be redefined to more sane and reasonable format.

    impl Encodable for RawTime {
        fn encode<S: Encoder>(&self, s: &mut S) -> Result<(), S::Error> {
            let (secs, frac) = self.to_serialized();
            s.emit_struct("RawTime", 2, |s| {
                try!(s.emit_struct_field("secs", 0, |s| secs.encode(s)));
                try!(s.emit_struct_field("frac", 1, |s| frac.encode(s)));
                Ok(())
            })
        }
    }

    impl Decodable for RawTime {
        fn decode<D: Decoder>(d: &mut D) -> Result<RawTime, D::Error> {
            d.read_struct("RawTime", 2, |d| {
                let secs = try!(d.read_struct_field("secs", 0, Decodable::decode));
                let frac = try!(d.read_struct_field("frac", 1, Decodable::decode));
                RawTime::from_serialized(secs, frac).ok_or_else(|| d.error("invalid time"))
            })
        }
    }

    #[test]
    fn test_encodable() {
        use rustc_serialize::json::encode;

        assert_eq!(encode(&RawTime::from_hms(0, 0, 0)).ok(),
                   Some(r#"{"secs":0,"frac":0}"#.into()));
        assert_eq!(encode(&RawTime::from_hms_milli(0, 0, 0, 950)).ok(),
                   Some(r#"{"secs":0,"frac":950000000}"#.into()));
        assert_eq!(encode(&RawTime::from_hms_milli(0, 0, 59, 1_000)).ok(),
                   Some(r#"{"secs":59,"frac":1000000000}"#.into()));
        assert_eq!(encode(&RawTime::from_hms(0, 1, 2)).ok(),
                   Some(r#"{"secs":62,"frac":0}"#.into()));
        assert_eq!(encode(&RawTime::from_hms(7, 8, 9)).ok(),
                   Some(r#"{"secs":25689,"frac":0}"#.into()));
        assert_eq!(encode(&RawTime::from_hms_micro(12, 34, 56, 789)).ok(),
                   Some(r#"{"secs":45296,"frac":789000}"#.into()));
        assert_eq!(encode(&RawTime::from_hms_nano(23, 59, 59, 1_999_999_999)).ok(),
                   Some(r#"{"secs":86399,"frac":1999999999}"#.into()));
    }

    #[test]
    fn test_decodable() {
        use rustc_serialize::json;

        let decode = |s: &str| json::decode::<RawTime>(s);

        assert_eq!(decode(r#"{"secs":0,"frac":0}"#).ok(),
                   Some(RawTime::from_hms(0, 0, 0)));
        assert_eq!(decode(r#"{"frac":950000000,"secs":0}"#).ok(),
                   Some(RawTime::from_hms_milli(0, 0, 0, 950)));
        assert_eq!(decode(r#"{"secs":59,"frac":1000000000}"#).ok(),
                   Some(RawTime::from_hms_milli(0, 0, 59, 1_000)));
        assert_eq!(decode(r#"{"frac": 0,
                              "secs": 62}"#).ok(),
                   Some(RawTime::from_hms(0, 1, 2)));
        assert_eq!(decode(r#"{"secs":25689,"frac":0}"#).ok(),
                   Some(RawTime::from_hms(7, 8, 9)));
        assert_eq!(decode(r#"{"secs":45296,"frac":789000}"#).ok(),
                   Some(RawTime::from_hms_micro(12, 34, 56, 789)));
        assert_eq!(decode(r#"{"secs":86399,"frac":1999999999}"#).ok(),
                   Some(RawTime::from_hms_nano(23, 59, 59, 1_999_999_999)));

        // bad formats
        assert!(decode(r#"{"secs":0,"frac":-1}"#).is_err());
        assert!(decode(r#"{"secs":-1,"frac":0}"#).is_err());
        assert!(decode(r#"{"secs":86400,"frac":0}"#).is_err());
        assert!(decode(r#"{"secs":0,"frac":2000000000}"#).is_err());
        assert!(decode(r#"{"secs":0}"#).is_err());
        assert!(decode(r#"{"frac":0}"#).is_err());
        assert!(decode(r#"{"secs":0.3,"frac":0}"#).is_err());
        assert!(decode(r#"{"secs":0,"frac":0.4}"#).is_err());
        assert!(decode(r#"{}"#).is_err());
        assert!(decode(r#"0"#).is_err());
        assert!(decode(r#"86399"#).is_err());
        assert!(decode(r#""string""#).is_err());
        assert!(decode(r#""12:34:56""#).is_err()); // :(
        assert!(decode(r#""12:34:56.789""#).is_err()); // :(
        assert!(decode(r#"null"#).is_err());
    }
}

#[cfg(feature = "serde")]
mod serde {
    use super::RawTime;
    use serde::{ser, de};

    // TODO not very optimized for space (binary formats would want something better)
    // TODO round-trip for general leap seconds (not just those with second = 60)

    impl ser::Serialize for RawTime {
        fn serialize<S>(&self, serializer: &mut S) -> Result<(), S::Error>
            where S: ser::Serializer
        {
            serializer.serialize_str(&format!("{:?}", self))
        }
    }

    struct RawTimeVisitor;

    impl de::Visitor for RawTimeVisitor {
        type Value = RawTime;

        fn visit_str<E>(&mut self, value: &str) -> Result<RawTime, E>
            where E: de::Error
        {
            value.parse().map_err(|err| E::custom(format!("{}", err)))
        }
    }

    impl de::Deserialize for RawTime {
        fn deserialize<D>(deserializer: &mut D) -> Result<Self, D::Error>
            where D: de::Deserializer
        {
            deserializer.deserialize_str(RawTimeVisitor)
        }
    }

    #[cfg(test)] extern crate serde_json;
    #[cfg(test)] extern crate bincode;

    #[test]
    fn test_serde_serialize() {
        use self::serde_json::to_string;

        assert_eq!(to_string(&RawTime::from_hms(0, 0, 0)).ok(),
                   Some(r#""00:00:00""#.into()));
        assert_eq!(to_string(&RawTime::from_hms_milli(0, 0, 0, 950)).ok(),
                   Some(r#""00:00:00.950""#.into()));
        assert_eq!(to_string(&RawTime::from_hms_milli(0, 0, 59, 1_000)).ok(),
                   Some(r#""00:00:60""#.into()));
        assert_eq!(to_string(&RawTime::from_hms(0, 1, 2)).ok(),
                   Some(r#""00:01:02""#.into()));
        assert_eq!(to_string(&RawTime::from_hms_nano(3, 5, 7, 98765432)).ok(),
                   Some(r#""03:05:07.098765432""#.into()));
        assert_eq!(to_string(&RawTime::from_hms(7, 8, 9)).ok(),
                   Some(r#""07:08:09""#.into()));
        assert_eq!(to_string(&RawTime::from_hms_micro(12, 34, 56, 789)).ok(),
                   Some(r#""12:34:56.000789""#.into()));
        assert_eq!(to_string(&RawTime::from_hms_nano(23, 59, 59, 1_999_999_999)).ok(),
                   Some(r#""23:59:60.999999999""#.into()));
    }

    #[test]
    fn test_serde_deserialize() {
        use self::serde_json::from_str;

        let from_str = |s: &str| serde_json::from_str::<RawTime>(s);

        assert_eq!(from_str(r#""00:00:00""#).ok(),
                   Some(RawTime::from_hms(0, 0, 0)));
        assert_eq!(from_str(r#""0:0:0""#).ok(),
                   Some(RawTime::from_hms(0, 0, 0)));
        assert_eq!(from_str(r#""00:00:00.950""#).ok(),
                   Some(RawTime::from_hms_milli(0, 0, 0, 950)));
        assert_eq!(from_str(r#""0:0:0.95""#).ok(),
                   Some(RawTime::from_hms_milli(0, 0, 0, 950)));
        assert_eq!(from_str(r#""00:00:60""#).ok(),
                   Some(RawTime::from_hms_milli(0, 0, 59, 1_000)));
        assert_eq!(from_str(r#""00:01:02""#).ok(),
                   Some(RawTime::from_hms(0, 1, 2)));
        assert_eq!(from_str(r#""03:05:07.098765432""#).ok(),
                   Some(RawTime::from_hms_nano(3, 5, 7, 98765432)));
        assert_eq!(from_str(r#""07:08:09""#).ok(),
                   Some(RawTime::from_hms(7, 8, 9)));
        assert_eq!(from_str(r#""12:34:56.000789""#).ok(),
                   Some(RawTime::from_hms_micro(12, 34, 56, 789)));
        assert_eq!(from_str(r#""23:59:60.999999999""#).ok(),
                   Some(RawTime::from_hms_nano(23, 59, 59, 1_999_999_999)));
        assert_eq!(from_str(r#""23:59:60.9999999999997""#).ok(), // excess digits are ignored
                   Some(RawTime::from_hms_nano(23, 59, 59, 1_999_999_999)));

        // bad formats
        assert!(from_str(r#""""#).is_err());
        assert!(from_str(r#""000000""#).is_err());
        assert!(from_str(r#""00:00:61""#).is_err());
        assert!(from_str(r#""00:60:00""#).is_err());
        assert!(from_str(r#""24:00:00""#).is_err());
        assert!(from_str(r#""23:59:59,1""#).is_err());
        assert!(from_str(r#""012:34:56""#).is_err());
        assert!(from_str(r#""hh:mm:ss""#).is_err());
        assert!(from_str(r#"0"#).is_err());
        assert!(from_str(r#"86399"#).is_err());
        assert!(from_str(r#"{}"#).is_err());
        assert!(from_str(r#"{"secs":0,"frac":0}"#).is_err()); // :(
        assert!(from_str(r#"null"#).is_err());
    }

    #[test]
    fn test_serde_bincode() {
        // Bincode is relevant to test separately from JSON because
        // it is not self-describing.
        use self::bincode::SizeLimit;
        use self::bincode::serde::{serialize, deserialize};

        let t = RawTime::from_hms_nano(3, 5, 7, 98765432);
        let encoded = serialize(&t, SizeLimit::Infinite).unwrap();
        let decoded: RawTime = deserialize(&encoded).unwrap();
        assert_eq!(t, decoded);
    }
}

#[cfg(test)]
mod tests {
    use super::RawTime;
    use Timelike;
    use duration::Duration;
    use std::u32;

    #[test]
    fn test_time_from_hms_milli() {
        assert_eq!(RawTime::from_hms_milli_opt(3, 5, 7, 0),
                   Some(RawTime::from_hms_nano(3, 5, 7, 0)));
        assert_eq!(RawTime::from_hms_milli_opt(3, 5, 7, 777),
                   Some(RawTime::from_hms_nano(3, 5, 7, 777_000_000)));
        assert_eq!(RawTime::from_hms_milli_opt(3, 5, 7, 1_999),
                   Some(RawTime::from_hms_nano(3, 5, 7, 1_999_000_000)));
        assert_eq!(RawTime::from_hms_milli_opt(3, 5, 7, 2_000), None);
        assert_eq!(RawTime::from_hms_milli_opt(3, 5, 7, 5_000), None); // overflow check
        assert_eq!(RawTime::from_hms_milli_opt(3, 5, 7, u32::MAX), None);
    }

    #[test]
    fn test_time_from_hms_micro() {
        assert_eq!(RawTime::from_hms_micro_opt(3, 5, 7, 0),
                   Some(RawTime::from_hms_nano(3, 5, 7, 0)));
        assert_eq!(RawTime::from_hms_micro_opt(3, 5, 7, 333),
                   Some(RawTime::from_hms_nano(3, 5, 7, 333_000)));
        assert_eq!(RawTime::from_hms_micro_opt(3, 5, 7, 777_777),
                   Some(RawTime::from_hms_nano(3, 5, 7, 777_777_000)));
        assert_eq!(RawTime::from_hms_micro_opt(3, 5, 7, 1_999_999),
                   Some(RawTime::from_hms_nano(3, 5, 7, 1_999_999_000)));
        assert_eq!(RawTime::from_hms_micro_opt(3, 5, 7, 2_000_000), None);
        assert_eq!(RawTime::from_hms_micro_opt(3, 5, 7, 5_000_000), None); // overflow check
        assert_eq!(RawTime::from_hms_micro_opt(3, 5, 7, u32::MAX), None);
    }

    #[test]
    fn test_time_hms() {
        assert_eq!(RawTime::from_hms(3, 5, 7).hour(), 3);
        assert_eq!(RawTime::from_hms(3, 5, 7).with_hour(0),
                   Some(RawTime::from_hms(0, 5, 7)));
        assert_eq!(RawTime::from_hms(3, 5, 7).with_hour(23),
                   Some(RawTime::from_hms(23, 5, 7)));
        assert_eq!(RawTime::from_hms(3, 5, 7).with_hour(24), None);
        assert_eq!(RawTime::from_hms(3, 5, 7).with_hour(u32::MAX), None);

        assert_eq!(RawTime::from_hms(3, 5, 7).minute(), 5);
        assert_eq!(RawTime::from_hms(3, 5, 7).with_minute(0),
                   Some(RawTime::from_hms(3, 0, 7)));
        assert_eq!(RawTime::from_hms(3, 5, 7).with_minute(59),
                   Some(RawTime::from_hms(3, 59, 7)));
        assert_eq!(RawTime::from_hms(3, 5, 7).with_minute(60), None);
        assert_eq!(RawTime::from_hms(3, 5, 7).with_minute(u32::MAX), None);

        assert_eq!(RawTime::from_hms(3, 5, 7).second(), 7);
        assert_eq!(RawTime::from_hms(3, 5, 7).with_second(0),
                   Some(RawTime::from_hms(3, 5, 0)));
        assert_eq!(RawTime::from_hms(3, 5, 7).with_second(59),
                   Some(RawTime::from_hms(3, 5, 59)));
        assert_eq!(RawTime::from_hms(3, 5, 7).with_second(60), None);
        assert_eq!(RawTime::from_hms(3, 5, 7).with_second(u32::MAX), None);
    }

    #[test]
    fn test_time_add() {
        macro_rules! check {
            ($lhs:expr, $rhs:expr, $sum:expr) => ({
                assert_eq!($lhs + $rhs, $sum);
                //assert_eq!($rhs + $lhs, $sum);
            })
        }

        let hmsm = |h,m,s,mi| RawTime::from_hms_milli(h, m, s, mi);

        check!(hmsm(3, 5, 7, 900), Duration::zero(), hmsm(3, 5, 7, 900));
        check!(hmsm(3, 5, 7, 900), Duration::milliseconds(100), hmsm(3, 5, 8, 0));
        check!(hmsm(3, 5, 7, 1_300), Duration::milliseconds(-1800), hmsm(3, 5, 6, 500));
        check!(hmsm(3, 5, 7, 1_300), Duration::milliseconds(-800), hmsm(3, 5, 7, 500));
        check!(hmsm(3, 5, 7, 1_300), Duration::milliseconds(-100), hmsm(3, 5, 7, 1_200));
        check!(hmsm(3, 5, 7, 1_300), Duration::milliseconds(100), hmsm(3, 5, 7, 1_400));
        check!(hmsm(3, 5, 7, 1_300), Duration::milliseconds(800), hmsm(3, 5, 8, 100));
        check!(hmsm(3, 5, 7, 1_300), Duration::milliseconds(1800), hmsm(3, 5, 9, 100));
        check!(hmsm(3, 5, 7, 900), Duration::seconds(86399), hmsm(3, 5, 6, 900)); // overwrap
        check!(hmsm(3, 5, 7, 900), Duration::seconds(-86399), hmsm(3, 5, 8, 900));
        check!(hmsm(3, 5, 7, 900), Duration::days(12345), hmsm(3, 5, 7, 900));
        check!(hmsm(3, 5, 7, 1_300), Duration::days(1), hmsm(3, 5, 7, 300));
        check!(hmsm(3, 5, 7, 1_300), Duration::days(-1), hmsm(3, 5, 8, 300));

        // regression tests for #37
        check!(hmsm(0, 0, 0, 0), Duration::milliseconds(-990), hmsm(23, 59, 59, 10));
        check!(hmsm(0, 0, 0, 0), Duration::milliseconds(-9990), hmsm(23, 59, 50, 10));
    }

    #[test]
    fn test_time_overflowing_add() {
        let hmsm = RawTime::from_hms_milli;

        assert_eq!(hmsm(3, 4, 5, 678).overflowing_add(Duration::hours(11)),
                   (hmsm(14, 4, 5, 678), 0));
        assert_eq!(hmsm(3, 4, 5, 678).overflowing_add(Duration::hours(23)),
                   (hmsm(2, 4, 5, 678), 86400));
        assert_eq!(hmsm(3, 4, 5, 678).overflowing_add(Duration::hours(-7)),
                   (hmsm(20, 4, 5, 678), -86400));

        // overflowing_add with leap seconds may be counter-intuitive
        assert_eq!(hmsm(3, 4, 5, 1_678).overflowing_add(Duration::days(1)),
                   (hmsm(3, 4, 5, 678), 86400));
        assert_eq!(hmsm(3, 4, 5, 1_678).overflowing_add(Duration::days(-1)),
                   (hmsm(3, 4, 6, 678), -86400));
    }

    #[test]
    fn test_time_sub() {
        macro_rules! check {
            ($lhs:expr, $rhs:expr, $diff:expr) => ({
                // `time1 - time2 = duration` is equivalent to `time2 - time1 = -duration`
                assert_eq!($lhs - $rhs, $diff);
                assert_eq!($rhs - $lhs, -$diff);
            })
        }

        let hmsm = |h,m,s,mi| RawTime::from_hms_milli(h, m, s, mi);

        check!(hmsm(3, 5, 7, 900), hmsm(3, 5, 7, 900), Duration::zero());
        check!(hmsm(3, 5, 7, 900), hmsm(3, 5, 7, 600), Duration::milliseconds(300));
        check!(hmsm(3, 5, 7, 200), hmsm(2, 4, 6, 200), Duration::seconds(3600 + 60 + 1));
        check!(hmsm(3, 5, 7, 200), hmsm(2, 4, 6, 300),
               Duration::seconds(3600 + 60) + Duration::milliseconds(900));

        // treats the leap second as if it coincides with the prior non-leap second,
        // as required by `time1 - time2 = duration` and `time2 - time1 = -duration` equivalence.
        check!(hmsm(3, 5, 7, 200), hmsm(3, 5, 6, 1_800), Duration::milliseconds(400));
        check!(hmsm(3, 5, 7, 1_200), hmsm(3, 5, 6, 1_800), Duration::milliseconds(1400));
        check!(hmsm(3, 5, 7, 1_200), hmsm(3, 5, 6, 800), Duration::milliseconds(1400));

        // additional equality: `time1 + duration = time2` is equivalent to
        // `time2 - time1 = duration` IF AND ONLY IF `time2` represents a non-leap second.
        assert_eq!(hmsm(3, 5, 6, 800) + Duration::milliseconds(400), hmsm(3, 5, 7, 200));
        assert_eq!(hmsm(3, 5, 6, 1_800) + Duration::milliseconds(400), hmsm(3, 5, 7, 200));
    }

    #[test]
    fn test_time_fmt() {
        assert_eq!(format!("{}", RawTime::from_hms_milli(23, 59, 59, 999)), "23:59:59.999");
        assert_eq!(format!("{}", RawTime::from_hms_milli(23, 59, 59, 1_000)), "23:59:60");
        assert_eq!(format!("{}", RawTime::from_hms_milli(23, 59, 59, 1_001)), "23:59:60.001");
        assert_eq!(format!("{}", RawTime::from_hms_micro(0, 0, 0, 43210)), "00:00:00.043210");
        assert_eq!(format!("{}", RawTime::from_hms_nano(0, 0, 0, 6543210)), "00:00:00.006543210");

        // the format specifier should have no effect on `RawTime`
        assert_eq!(format!("{:30}", RawTime::from_hms_milli(3, 5, 7, 9)), "03:05:07.009");
    }

    #[test]
    fn test_date_from_str() {
        // valid cases
        let valid = [
            "0:0:0",
            "0:0:0.0000000",
            "0:0:0.0000003",
            " 4 : 3 : 2.1 ",
            " 09:08:07 ",
            " 9:8:07 ",
            "23:59:60.373929310237",
        ];
        for &s in &valid {
            let d = match s.parse::<RawTime>() {
                Ok(d) => d,
                Err(e) => panic!("parsing `{}` has failed: {}", s, e)
            };
            let s_ = format!("{:?}", d);
            // `s` and `s_` may differ, but `s.parse()` and `s_.parse()` must be same
            let d_ = match s_.parse::<RawTime>() {
                Ok(d) => d,
                Err(e) => panic!("`{}` is parsed into `{:?}`, but reparsing that has failed: {}",
                                 s, d, e)
            };
            assert!(d == d_, "`{}` is parsed into `{:?}`, but reparsed result \
                              `{:?}` does not match", s, d, d_);
        }

        // some invalid cases
        // since `ParseErrorKind` is private, all we can do is to check if there was an error
        assert!("".parse::<RawTime>().is_err());
        assert!("x".parse::<RawTime>().is_err());
        assert!("15".parse::<RawTime>().is_err());
        assert!("15:8".parse::<RawTime>().is_err());
        assert!("15:8:x".parse::<RawTime>().is_err());
        assert!("15:8:9x".parse::<RawTime>().is_err());
        assert!("23:59:61".parse::<RawTime>().is_err());
        assert!("12:34:56.x".parse::<RawTime>().is_err());
        assert!("12:34:56. 0".parse::<RawTime>().is_err());
    }

    #[test]
    fn test_time_parse_from_str() {
        let hms = |h,m,s| RawTime::from_hms(h,m,s);
        assert_eq!(RawTime::parse_from_str("2014-5-7T12:34:56+09:30", "%Y-%m-%dT%H:%M:%S%z"),
                   Ok(hms(12, 34, 56))); // ignore date and offset
        assert_eq!(RawTime::parse_from_str("PM 12:59", "%P %H:%M"),
                   Ok(hms(12, 59, 0)));
        assert!(RawTime::parse_from_str("12:3456", "%H:%M:%S").is_err());
    }

    #[test]
    fn test_time_format() {
        let t = RawTime::from_hms_nano(3, 5, 7, 98765432);
        assert_eq!(t.format("%H,%k,%I,%l,%P,%p").to_string(), "03, 3,03, 3,am,AM");
        assert_eq!(t.format("%M").to_string(), "05");
        assert_eq!(t.format("%S,%f,%.f").to_string(), "07,098765432,.098765432");
        assert_eq!(t.format("%.3f,%.6f,%.9f").to_string(), ".098,.098765,.098765432");
        assert_eq!(t.format("%R").to_string(), "03:05");
        assert_eq!(t.format("%T,%X").to_string(), "03:05:07,03:05:07");
        assert_eq!(t.format("%r").to_string(), "03:05:07 AM");
        assert_eq!(t.format("%t%n%%%n%t").to_string(), "\t\n%\n\t");

        let t = RawTime::from_hms_micro(3, 5, 7, 432100);
        assert_eq!(t.format("%S,%f,%.f").to_string(), "07,432100000,.432100");
        assert_eq!(t.format("%.3f,%.6f,%.9f").to_string(), ".432,.432100,.432100000");

        let t = RawTime::from_hms_milli(3, 5, 7, 210);
        assert_eq!(t.format("%S,%f,%.f").to_string(), "07,210000000,.210");
        assert_eq!(t.format("%.3f,%.6f,%.9f").to_string(), ".210,.210000,.210000000");

        let t = RawTime::from_hms(3, 5, 7);
        assert_eq!(t.format("%S,%f,%.f").to_string(), "07,000000000,");
        assert_eq!(t.format("%.3f,%.6f,%.9f").to_string(), ".000,.000000,.000000000");

        // corner cases
        assert_eq!(RawTime::from_hms(13, 57, 9).format("%r").to_string(), "01:57:09 PM");
        assert_eq!(RawTime::from_hms_milli(23, 59, 59, 1_000).format("%X").to_string(),
                   "23:59:60");
    }
}

