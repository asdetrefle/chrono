// Copyright (c) 2016-2017, Qijie Chu.

//! ISO 8601 date and time without timezone.

use std::{str, fmt, hash};
use std::ops::{Add, Sub};

use prelude::*;
use super::rawtime::RawTime;
use super::rawdate::RawDate;
//use format::{Item, Numeric, Pad, Fixed};
//use format::{parse, Parsed, ParseError, ParseResult, DelayedFormat, StrftimeItems};

/// The tight upper bound guarantees that a duration with `|Duration| >= 2^MAX_SECS_BITS`
/// will always overflow the addition with any date and time type.
///
/// So why is this needed? `Duration::seconds(rhs)` may overflow, and we don't have
/// an alternative returning `Option` or `Result`. Thus we need some early bound to avoid
/// touching that call when we are already sure that it WILL overflow...
const MAX_SECS_BITS: usize = 44;

/// ISO 8601 combined date and time without timezone.
///
/// # Example
///
/// `RawDateTime` is commonly created from [`RawDate`](../date/struct.RawDate.html).
///
/// ~~~~
/// use chrono::{RawDate, RawDateTime};
///
/// let dt: RawDateTime = RawDate::from_ymd(2016, 7, 8).and_hms(9, 10, 11);
/// # let _ = dt;
/// ~~~~
///
/// You can use typical [date-like](../../trait.Datelike.html) and
/// [time-like](../../trait.Timelike.html) methods,
/// provided that relevant traits are in the scope.
///
/// ~~~~
/// # use chrono::{RawDate, RawDateTime};
/// # let dt: RawDateTime = RawDate::from_ymd(2016, 7, 8).and_hms(9, 10, 11);
/// use chrono::{Datelike, Timelike, Weekday};
///
/// assert_eq!(dt.weekday(), Weekday::Fri);
/// assert_eq!(dt.num_seconds_from_midnight(), 33011);
/// ~~~~
#[derive(PartialEq, Eq, PartialOrd, Ord, Copy, Clone)]
pub struct RawDateTime {
    date: RawDate,
    time: RawTime,
}

impl RawDateTime {
    // Makes a new `RawDateTime` from date and time components.
    // Equivalent to [`date.and_time(time)`](../date/struct.RawDate.html#method.and_time)
    // and many other helper constructors on `RawDate`.
    #[inline]
    pub fn new(date: RawDate, time: RawTime) -> Option<RawDateTime> {
        Some(RawDateTime { date: date, time: time })
    }

    // Makes a new `RawDateTime` corresponding to a UTC date and time,
    // from the number of non-leap seconds
    // since the midnight UTC on January 1, 1970 (aka "UNIX timestamp")
    // and the number of nanoseconds since the last whole non-leap second.
    // The nanosecond part can exceed 1,000,000,000
    // in order to represent the [leap second](../time/index.html#leap-second-handling).
    // (The true "UNIX timestamp" cannot represent a leap second unambiguously.)
    // Returns `None` on the out-of-range number of seconds and/or invalid nanosecond.
    #[inline]
    pub fn from_secs(secs: i64, nsecs: u32) -> Option<RawDateTime> {
        let (days, secs) = div_mod_floor(secs, 86400);
        let date = (days as i32).checked_add(719163)
                                .and_then(|days_ce| RawDate::from_num_days_from_ce_opt(days_ce));
        let time = RawTime::new(secs as u32, nsecs);
        match (date, time) {
            (Some(date), Some(time)) => Some(RawDateTime { date: date, time: time }),
            (_, _) => None,
        }
    }

    // Returns the number of non-leap seconds since the midnight on January 1, 1970.
    // Note that this does *not* account for the timezone!
    // The true "UNIX timestamp" would count seconds since the midnight *UTC* on the epoch.
    #[inline]
    pub fn into_secs(&self) -> i64 {
        let ndays = self.date.num_days_from_ce() as i64;
        let nseconds = self.time.second() as i64;
        (ndays - 719163) * 86400 + nseconds
    }
    
    #[inline]
    pub fn date(&self) -> RawDate {
        self.date
    }

    #[inline]
    pub fn time(&self) -> RawTime {
        self.time
    }

    // Adds given `Duration` to the current date and time.
    // As a part of Chrono's [leap second handling](../time/index.html#leap-second-handling),
    // the addition assumes that **there is no leap second ever**,
    // except when the `RawDateTime` itself represents a leap second
    // in which case the assumption becomes that **there is exactly a single leap second ever**.
    // Returns `None` when it will result in overflow.
    pub fn checked_add(self, rhs: Duration) -> Option<RawDateTime> {
        let (time, rhs) = self.time.overflowing_add(rhs);

        // early checking to avoid overflow in Duration::seconds
        if rhs <= (-1 << MAX_SECS_BITS) || rhs >= (1 << MAX_SECS_BITS) {
            return None;
        }

        let date = self.date.checked_add(Duration::seconds(rhs)).expect("Duration Overflow");
        Some(RawDateTime { date: date, time: time })
    }

    // Subtracts given `Duration` from the current date and time.
    // As a part of Chrono's [leap second handling](../time/index.html#leap-second-handling),
    // the subtraction assumes that **there is no leap second ever**,
    // except when the `RawDateTime` itself represents a leap second
    // in which case the assumption becomes that **there is exactly a single leap second ever**.
    // Returns `None` when it will result in overflow.
    pub fn checked_sub(self, rhs: Duration) -> Option<RawDateTime> {
        let (time, rhs) = self.time.overflowing_sub(rhs);

        // early checking to avoid overflow in Duration::seconds
        if rhs <= (-1 << MAX_SECS_BITS) || rhs >= (1 << MAX_SECS_BITS) {
            return None;
        }

        let date = self.date.checked_sub(Duration::seconds(rhs)).expect("Duration Overflow");
        Some(RawDateTime { date: date, time: time })
    }

    /*
    /// Parses a string with the specified format string and returns a new `RawDateTime`.
    /// See the [`format::strftime` module](../../format/strftime/index.html)
    /// on the supported escape sequences.
    ///
    /// # Example
    ///
    /// ~~~~
    /// use chrono::{RawDateTime, RawDate};
    ///
    /// let parse_from_str = RawDateTime::parse_from_str;
    ///
    /// assert_eq!(parse_from_str("2015-09-05 23:56:04", "%Y-%m-%d %H:%M:%S"),
    ///            Ok(RawDate::from_ymd(2015, 9, 5).and_hms(23, 56, 4)));
    /// assert_eq!(parse_from_str("5sep2015pm012345.6789", "%d%b%Y%p%I%M%S%.f"),
    ///            Ok(RawDate::from_ymd(2015, 9, 5).and_hms_micro(13, 23, 45, 678_900)));
    /// ~~~~
    ///
    /// Offset is ignored for the purpose of parsing.
    ///
    /// ~~~~
    /// # use chrono::{RawDateTime, RawDate};
    /// # let parse_from_str = RawDateTime::parse_from_str;
    /// assert_eq!(parse_from_str("2014-5-17T12:34:56+09:30", "%Y-%m-%dT%H:%M:%S%z"),
    ///            Ok(RawDate::from_ymd(2014, 5, 17).and_hms(12, 34, 56)));
    /// ~~~~
    ///
    /// [Leap seconds](./index.html#leap-second-handling) are correctly handled by
    /// treating any time of the form `hh:mm:60` as a leap second.
    /// (This equally applies to the formatting, so the round trip is possible.)
    ///
    /// ~~~~
    /// # use chrono::{RawDateTime, RawDate};
    /// # let parse_from_str = RawDateTime::parse_from_str;
    /// assert_eq!(parse_from_str("2015-07-01 08:59:60.123", "%Y-%m-%d %H:%M:%S%.f"),
    ///            Ok(RawDate::from_ymd(2015, 7, 1).and_hms_milli(8, 59, 59, 1_123)));
    /// ~~~~
    ///
    /// Missing seconds are assumed to be zero,
    /// but out-of-bound times or insufficient fields are errors otherwise.
    ///
    /// ~~~~
    /// # use chrono::{RawDateTime, RawDate};
    /// # let parse_from_str = RawDateTime::parse_from_str;
    /// assert_eq!(parse_from_str("94/9/4 7:15", "%y/%m/%d %H:%M"),
    ///            Ok(RawDate::from_ymd(1994, 9, 4).and_hms(7, 15, 0)));
    ///
    /// assert!(parse_from_str("04m33s", "%Mm%Ss").is_err());
    /// assert!(parse_from_str("94/9/4 12", "%y/%m/%d %H").is_err());
    /// assert!(parse_from_str("94/9/4 17:60", "%y/%m/%d %H:%M").is_err());
    /// assert!(parse_from_str("94/9/4 24:00:00", "%y/%m/%d %H:%M:%S").is_err());
    /// ~~~~
    ///
    /// All parsed fields should be consistent to each other, otherwise it's an error.
    ///
    /// ~~~~
    /// # use chrono::RawDateTime;
    /// # let parse_from_str = RawDateTime::parse_from_str;
    /// let fmt = "%Y-%m-%d %H:%M:%S = UNIX timestamp %s";
    /// assert!(parse_from_str("2001-09-09 01:46:39 = UNIX timestamp 999999999", fmt).is_ok());
    /// assert!(parse_from_str("1970-01-01 00:00:00 = UNIX timestamp 1", fmt).is_err());
    /// ~~~~
    pub fn parse_from_str(s: &str, fmt: &str) -> ParseResult<RawDateTime> {
        let mut parsed = Parsed::new();
        try!(parse(&mut parsed, s, StrftimeItems::new(fmt)));
        parsed.to_raw_datetime_with_offset(0) // no offset adjustment
    }

    /// Formats the combined date and time with the specified formatting items.
    /// Otherwise it is same to the ordinary [`format`](#method.format) method.
    ///
    /// The `Iterator` of items should be `Clone`able,
    /// since the resulting `DelayedFormat` value may be formatted multiple times.
    ///
    /// # Example
    ///
    /// ~~~~
    /// use chrono::RawDate;
    /// use chrono::format::strftime::StrftimeItems;
    ///
    /// let fmt = StrftimeItems::new("%Y-%m-%d %H:%M:%S");
    /// let dt = RawDate::from_ymd(2015, 9, 5).and_hms(23, 56, 4);
    /// assert_eq!(dt.format_with_items(fmt.clone()).to_string(), "2015-09-05 23:56:04");
    /// assert_eq!(dt.format("%Y-%m-%d %H:%M:%S").to_string(),    "2015-09-05 23:56:04");
    /// ~~~~
    ///
    /// The resulting `DelayedFormat` can be formatted directly via the `Display` trait.
    ///
    /// ~~~~
    /// # use chrono::RawDate;
    /// # use chrono::format::strftime::StrftimeItems;
    /// # let fmt = StrftimeItems::new("%Y-%m-%d %H:%M:%S").clone();
    /// # let dt = RawDate::from_ymd(2015, 9, 5).and_hms(23, 56, 4);
    /// assert_eq!(format!("{}", dt.format_with_items(fmt)), "2015-09-05 23:56:04");
    /// ~~~~
    #[inline]
    pub fn format_with_items<'a, I>(&self, items: I) -> DelayedFormat<I>
            where I: Iterator<Item=Item<'a>> + Clone {
        DelayedFormat::new(Some(self.date.clone()), Some(self.time.clone()), items)
    }

    /// Formats the combined date and time with the specified format string.
    /// See the [`format::strftime` module](../../format/strftime/index.html)
    /// on the supported escape sequences.
    ///
    /// This returns a `DelayedFormat`,
    /// which gets converted to a string only when actual formatting happens.
    /// You may use the `to_string` method to get a `String`,
    /// or just feed it into `print!` and other formatting macros.
    /// (In this way it avoids the redundant memory allocation.)
    ///
    /// A wrong format string does *not* issue an error immediately.
    /// Rather, converting or formatting the `DelayedFormat` fails.
    /// You are recommended to immediately use `DelayedFormat` for this reason.
    ///
    /// # Example
    ///
    /// ~~~~
    /// use chrono::RawDate;
    ///
    /// let dt = RawDate::from_ymd(2015, 9, 5).and_hms(23, 56, 4);
    /// assert_eq!(dt.format("%Y-%m-%d %H:%M:%S").to_string(), "2015-09-05 23:56:04");
    /// assert_eq!(dt.format("around %l %p on %b %-d").to_string(), "around 11 PM on Sep 5");
    /// ~~~~
    ///
    /// The resulting `DelayedFormat` can be formatted directly via the `Display` trait.
    ///
    /// ~~~~
    /// # use chrono::RawDate;
    /// # let dt = RawDate::from_ymd(2015, 9, 5).and_hms(23, 56, 4);
    /// assert_eq!(format!("{}", dt.format("%Y-%m-%d %H:%M:%S")), "2015-09-05 23:56:04");
    /// assert_eq!(format!("{}", dt.format("around %l %p on %b %-d")), "around 11 PM on Sep 5");
    /// ~~~~
    #[inline]
    pub fn format<'a>(&self, fmt: &'a str) -> DelayedFormat<StrftimeItems<'a>> {
        self.format_with_items(StrftimeItems::new(fmt))
    }
    */
}

impl Datelike for RawDateTime {
    #[inline]
    fn year(&self) -> i32 {
        self.date.year()
    }

    #[inline]
    fn month(&self) -> u32 {
        self.date.month()
    }

    #[inline]
    fn day(&self) -> u32 {
        self.date.day()
    }

    #[inline]
    fn ordinal(&self) -> u32 {
        self.date.ordinal()
    }

    #[inline]
    fn weekday(&self) -> Weekday {
        self.date.weekday()
    }

    #[inline]
    fn isoweekdate(&self) -> (i32, u32, Weekday) {
        self.date.isoweekdate()
    }

    #[inline]
    fn with_year(&self, year: i32) -> Option<RawDateTime> {
        self.date.with_year(year).map(|d| RawDateTime { date: d, ..*self })
    }

    #[inline]
    fn with_month(&self, month: u32) -> Option<RawDateTime> {
        self.date.with_month(month).map(|d| RawDateTime { date: d, ..*self })
    }

    #[inline]
    fn with_day(&self, day: u32) -> Option<RawDateTime> {
        self.date.with_day(day).map(|d| RawDateTime { date: d, ..*self })
    }

    #[inline]
    fn with_ordinal(&self, ordinal: u32) -> Option<RawDateTime> {
        self.date.with_ordinal(ordinal).map(|d| RawDateTime { date: d, ..*self })
    }
}

impl Timelike for RawDateTime {
    #[inline]
    fn hour(&self) -> u32 {
        self.time.hour()
    }

    #[inline]
    fn minute(&self) -> u32 {
        self.time.minute()
    }

    #[inline]
    fn second(&self) -> u32 {
        self.time.second()
    }

    #[inline]
    fn timestamp(&self) -> f64 {
        self.time.timestamp()
    }

    #[inline]
    fn nanosecond(&self) -> u32 {
        self.time.nanosecond()
    }

    #[inline]
    fn with_hour(&self, hour: u32) -> Option<RawDateTime> {
        self.time.with_hour(hour).map(|t| RawDateTime { time: t, ..*self })
    }

    #[inline]
    fn with_minute(&self, min: u32) -> Option<RawDateTime> {
        self.time.with_minute(min).map(|t| RawDateTime { time: t, ..*self })
    }

    #[inline]
    fn with_second(&self, sec: u32) -> Option<RawDateTime> {
        self.time.with_second(sec).map(|t| RawDateTime { time: t, ..*self })
    }

    #[inline]
    fn with_nanosecond(&self, nano: u32) -> Option<RawDateTime> {
        self.time.with_nanosecond(nano).map(|t| RawDateTime { time: t, ..*self })
    }
}

// `RawDateTime` can be used as a key to the hash maps (in principle).
// Practically this also takes account of fractional seconds, so it is not recommended.
// (For the obvious reason this also distinguishes leap seconds from non-leap seconds.)
impl hash::Hash for RawDateTime {
    fn hash<H: hash::Hasher>(&self, state: &mut H) {
        self.date.hash(state);
        self.time.hash(state);
    }
}

impl Add<Duration> for RawDateTime {
    type Output = RawDateTime;

    #[inline]
    fn add(self, rhs: Duration) -> RawDateTime {
        self.checked_add(rhs).expect("`RawDateTime + Duration` overflowed")
    }
}

impl Sub<RawDateTime> for RawDateTime {
    type Output = Duration;

    fn sub(self, rhs: RawDateTime) -> Duration {
        (self.date - rhs.date) + (self.time - rhs.time)
    }
}

impl Sub<Duration> for RawDateTime {
    type Output = RawDateTime;

    #[inline]
    fn sub(self, rhs: Duration) -> RawDateTime {
        self.checked_sub(rhs).expect("`RawDateTime - Duration` overflowed")
    }
}

/// The `Debug` output of the raw date and time `dt` is same to
/// [`dt.format("%Y-%m-%dT%H:%M:%S%.f")`](../../format/strftime/index.html).
///
/// The string printed can be readily parsed via the `parse` method on `str`.
///
/// It should be noted that, for leap seconds not on the minute boundary,
/// it may print a representation not distinguishable from non-leap seconds.
/// This doesn't matter in practice, since such leap seconds never happened.
/// (By the time of the first leap second on 1972-06-30,
/// every time zone offset around the world has standardized to the 5-minute alignment.)
///
/// # Example
///
/// ~~~~
/// use chrono::RawDate;
///
/// let dt = RawDate::from_ymd(2016, 11, 15).and_hms(7, 39, 24);
/// assert_eq!(format!("{:?}", dt), "2016-11-15T07:39:24");
/// ~~~~
///
/// Leap seconds may also be used.
///
/// ~~~~
/// # use chrono::RawDate;
/// let dt = RawDate::from_ymd(2015, 6, 30).and_hms_milli(23, 59, 59, 1_500);
/// assert_eq!(format!("{:?}", dt), "2015-06-30T23:59:60.500");
/// ~~~~
impl fmt::Debug for RawDateTime {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?} {:?}", self.date, self.time)
    }
}

/// The `Debug` output of the raw date and time `dt` is same to
/// [`dt.format("%Y-%m-%d %H:%M:%S%.f")`](../../format/strftime/index.html).
///
/// It should be noted that, for leap seconds not on the minute boundary,
/// it may print a representation not distinguishable from non-leap seconds.
/// This doesn't matter in practice, since such leap seconds never happened.
/// (By the time of the first leap second on 1972-06-30,
/// every time zone offset around the world has standardized to the 5-minute alignment.)
///
/// # Example
///
/// ~~~~
/// use chrono::RawDate;
///
/// let dt = RawDate::from_ymd(2016, 11, 15).and_hms(7, 39, 24);
/// assert_eq!(format!("{}", dt), "2016-11-15 07:39:24");
/// ~~~~
///
/// Leap seconds may also be used.
///
/// ~~~~
/// # use chrono::RawDate;
/// let dt = RawDate::from_ymd(2015, 6, 30).and_hms_milli(23, 59, 59, 1_500);
/// assert_eq!(format!("{}", dt), "2015-06-30 23:59:60.500");
/// ~~~~
impl fmt::Display for RawDateTime {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}T{}", self.date, self.time)
    }
}

/*
/// Parsing a `str` into a `RawDateTime` uses the same format,
/// [`%Y-%m-%dT%H:%M:%S%.f`](../../format/strftime/index.html), as in `Debug`.
///
/// # Example
///
/// ~~~~
/// use chrono::{RawDateTime, RawDate};
///
/// let dt = RawDate::from_ymd(2015, 9, 18).and_hms(23, 56, 4);
/// assert_eq!("2015-09-18T23:56:04".parse::<RawDateTime>(), Ok(dt));
///
/// let dt = RawDate::from_ymd(12345, 6, 7).and_hms_milli(7, 59, 59, 1_500); // leap second
/// assert_eq!("+12345-6-7T7:59:60.5".parse::<RawDateTime>(), Ok(dt));
///
/// assert!("foo".parse::<RawDateTime>().is_err());
/// ~~~~
impl str::FromStr for RawDateTime {
    type Err = ParseError;

    fn from_str(s: &str) -> ParseResult<RawDateTime> {
        const ITEMS: &'static [Item<'static>] = &[
            Item::Space(""), Item::Numeric(Numeric::Year, Pad::Zero),
            Item::Space(""), Item::Literal("-"),
            Item::Space(""), Item::Numeric(Numeric::Month, Pad::Zero),
            Item::Space(""), Item::Literal("-"),
            Item::Space(""), Item::Numeric(Numeric::Day, Pad::Zero),
            Item::Space(""), Item::Literal("T"), // XXX shouldn't this be case-insensitive?
            Item::Space(""), Item::Numeric(Numeric::Hour, Pad::Zero),
            Item::Space(""), Item::Literal(":"),
            Item::Space(""), Item::Numeric(Numeric::Minute, Pad::Zero),
            Item::Space(""), Item::Literal(":"),
            Item::Space(""), Item::Numeric(Numeric::Second, Pad::Zero),
            Item::Fixed(Fixed::Nanosecond), Item::Space(""),
        ];

        let mut parsed = Parsed::new();
        try!(parse(&mut parsed, s, ITEMS.iter().cloned()));
        parsed.to_raw_datetime_with_offset(0)
    }
}
*/

#[cfg(feature = "rustc-serialize")]
mod rustc_serialize {
    use super::RawDateTime;
    use rustc_serialize::{Encodable, Encoder, Decodable, Decoder};

    // TODO the current serialization format is NEVER intentionally defined.
    // in the future it is likely to be redefined to more sane and reasonable format.

    impl Encodable for RawDateTime {
        fn encode<S: Encoder>(&self, s: &mut S) -> Result<(), S::Error> {
            s.emit_struct("RawDateTime", 2, |s| {
                try!(s.emit_struct_field("date", 0, |s| self.date.encode(s)));
                try!(s.emit_struct_field("time", 1, |s| self.time.encode(s)));
                Ok(())
            })
        }
    }

    impl Decodable for RawDateTime {
        fn decode<D: Decoder>(d: &mut D) -> Result<RawDateTime, D::Error> {
            d.read_struct("RawDateTime", 2, |d| {
                let date = try!(d.read_struct_field("date", 0, Decodable::decode));
                let time = try!(d.read_struct_field("time", 1, Decodable::decode));
                Ok(RawDateTime::new(date, time))
            })
        }
    }

    #[test]
    fn test_encodable() {
        use raw::date::{self, RawDate};
        use rustc_serialize::json::encode;

        assert_eq!(
            encode(&RawDate::from_ymd(2016, 7, 8).and_hms_milli(9, 10, 48, 90)).ok(),
            Some(r#"{"date":{"ymdf":16518115},"time":{"secs":33048,"frac":90000000}}"#.into()));
        assert_eq!(
            encode(&RawDate::from_ymd(2014, 7, 24).and_hms(12, 34, 6)).ok(),
            Some(r#"{"date":{"ymdf":16501977},"time":{"secs":45246,"frac":0}}"#.into()));
        assert_eq!(
            encode(&RawDate::from_ymd(0, 1, 1).and_hms_milli(0, 0, 59, 1_000)).ok(),
            Some(r#"{"date":{"ymdf":20},"time":{"secs":59,"frac":1000000000}}"#.into()));
        assert_eq!(
            encode(&RawDate::from_ymd(-1, 12, 31).and_hms_nano(23, 59, 59, 7)).ok(),
            Some(r#"{"date":{"ymdf":-2341},"time":{"secs":86399,"frac":7}}"#.into()));
        assert_eq!(
            encode(&date::MIN.and_hms(0, 0, 0)).ok(),
            Some(r#"{"date":{"ymdf":-2147483625},"time":{"secs":0,"frac":0}}"#.into()));
        assert_eq!(
            encode(&date::MAX.and_hms_nano(23, 59, 59, 1_999_999_999)).ok(),
            Some(r#"{"date":{"ymdf":2147481311},"time":{"secs":86399,"frac":1999999999}}"#.into()));
    }

    #[test]
    fn test_decodable() {
        use raw::date::{self, RawDate};
        use rustc_serialize::json;

        let decode = |s: &str| json::decode::<RawDateTime>(s);

        assert_eq!(
            decode(r#"{"date":{"ymdf":16518115},"time":{"secs":33048,"frac":90000000}}"#).ok(),
            Some(RawDate::from_ymd(2016, 7, 8).and_hms_milli(9, 10, 48, 90)));
        assert_eq!(
            decode(r#"{"time":{"frac":0,"secs":45246},"date":{"ymdf":16501977}}"#).ok(),
            Some(RawDate::from_ymd(2014, 7, 24).and_hms(12, 34, 6)));
        assert_eq!(
            decode(r#"{"date": {"ymdf": 20},
                       "time": {"secs": 59,
                                "frac": 1000000000}}"#).ok(),
            Some(RawDate::from_ymd(0, 1, 1).and_hms_milli(0, 0, 59, 1_000)));
        assert_eq!(
            decode(r#"{"date":{"ymdf":-2341},"time":{"secs":86399,"frac":7}}"#).ok(),
            Some(RawDate::from_ymd(-1, 12, 31).and_hms_nano(23, 59, 59, 7)));
        assert_eq!(
            decode(r#"{"date":{"ymdf":-2147483625},"time":{"secs":0,"frac":0}}"#).ok(),
            Some(date::MIN.and_hms(0, 0, 0)));
        assert_eq!(
            decode(r#"{"date":{"ymdf":2147481311},"time":{"secs":86399,"frac":1999999999}}"#).ok(),
            Some(date::MAX.and_hms_nano(23, 59, 59, 1_999_999_999)));

        // bad formats
        assert!(decode(r#"{"date":{},"time":{}}"#).is_err());
        assert!(decode(r#"{"date":{"ymdf":0},"time":{"secs":0,"frac":0}}"#).is_err());
        assert!(decode(r#"{"date":{"ymdf":20},"time":{"secs":86400,"frac":0}}"#).is_err());
        assert!(decode(r#"{"date":{"ymdf":20},"time":{"secs":0,"frac":-1}}"#).is_err());
        assert!(decode(r#"{"date":20,"time":{"secs":0,"frac":0}}"#).is_err());
        assert!(decode(r#"{"date":"2016-08-04","time":"01:02:03.456"}"#).is_err());
        assert!(decode(r#"{"date":{"ymdf":20}}"#).is_err());
        assert!(decode(r#"{"time":{"secs":0,"frac":0}}"#).is_err());
        assert!(decode(r#"{"ymdf":20}"#).is_err());
        assert!(decode(r#"{"secs":0,"frac":0}"#).is_err());
        assert!(decode(r#"{}"#).is_err());
        assert!(decode(r#"0"#).is_err());
        assert!(decode(r#"-1"#).is_err());
        assert!(decode(r#""string""#).is_err());
        assert!(decode(r#""2016-08-04T12:34:56""#).is_err()); // :(
        assert!(decode(r#""2016-08-04T12:34:56.789""#).is_err()); // :(
        assert!(decode(r#"null"#).is_err());
    }
}

#[cfg(feature = "serde")]
mod serde {
    use super::RawDateTime;
    use serde::{ser, de};

    // TODO not very optimized for space (binary formats would want something better)

    impl ser::Serialize for RawDateTime {
        fn serialize<S>(&self, serializer: &mut S) -> Result<(), S::Error>
            where S: ser::Serializer
        {
            serializer.serialize_str(&format!("{:?}", self))
        }
    }

    struct RawDateTimeVisitor;

    impl de::Visitor for RawDateTimeVisitor {
        type Value = RawDateTime;

        fn visit_str<E>(&mut self, value: &str) -> Result<RawDateTime, E>
            where E: de::Error
        {
            value.parse().map_err(|err| E::custom(format!("{}", err)))
        }
    }

    impl de::Deserialize for RawDateTime {
        fn deserialize<D>(deserializer: &mut D) -> Result<Self, D::Error>
            where D: de::Deserializer
        {
            deserializer.deserialize_str(RawDateTimeVisitor)
        }
    }

    #[cfg(test)] extern crate serde_json;
    #[cfg(test)] extern crate bincode;

    #[test]
    fn test_serde_serialize() {
        use raw::date::{self, RawDate};
        use self::serde_json::to_string;

        assert_eq!(
            to_string(&RawDate::from_ymd(2016, 7, 8).and_hms_milli(9, 10, 48, 90)).ok(),
            Some(r#""2016-07-08T09:10:48.090""#.into()));
        assert_eq!(
            to_string(&RawDate::from_ymd(2014, 7, 24).and_hms(12, 34, 6)).ok(),
            Some(r#""2014-07-24T12:34:06""#.into()));
        assert_eq!(
            to_string(&RawDate::from_ymd(0, 1, 1).and_hms_milli(0, 0, 59, 1_000)).ok(),
            Some(r#""0000-01-01T00:00:60""#.into()));
        assert_eq!(
            to_string(&RawDate::from_ymd(-1, 12, 31).and_hms_nano(23, 59, 59, 7)).ok(),
            Some(r#""-0001-12-31T23:59:59.000000007""#.into()));
        assert_eq!(
            to_string(&date::MIN.and_hms(0, 0, 0)).ok(),
            Some(r#""-262144-01-01T00:00:00""#.into()));
        assert_eq!(
            to_string(&date::MAX.and_hms_nano(23, 59, 59, 1_999_999_999)).ok(),
            Some(r#""+262143-12-31T23:59:60.999999999""#.into()));
    }

    #[test]
    fn test_serde_deserialize() {
        use raw::date::{self, RawDate};
        use self::serde_json::from_str;

        let from_str = |s: &str| serde_json::from_str::<RawDateTime>(s);

        assert_eq!(
            from_str(r#""2016-07-08T09:10:48.090""#).ok(),
            Some(RawDate::from_ymd(2016, 7, 8).and_hms_milli(9, 10, 48, 90)));
        assert_eq!(
            from_str(r#""2016-7-8T9:10:48.09""#).ok(),
            Some(RawDate::from_ymd(2016, 7, 8).and_hms_milli(9, 10, 48, 90)));
        assert_eq!(
            from_str(r#""2014-07-24T12:34:06""#).ok(),
            Some(RawDate::from_ymd(2014, 7, 24).and_hms(12, 34, 6)));
        assert_eq!(
            from_str(r#""0000-01-01T00:00:60""#).ok(),
            Some(RawDate::from_ymd(0, 1, 1).and_hms_milli(0, 0, 59, 1_000)));
        assert_eq!(
            from_str(r#""0-1-1T0:0:60""#).ok(),
            Some(RawDate::from_ymd(0, 1, 1).and_hms_milli(0, 0, 59, 1_000)));
        assert_eq!(
            from_str(r#""-0001-12-31T23:59:59.000000007""#).ok(),
            Some(RawDate::from_ymd(-1, 12, 31).and_hms_nano(23, 59, 59, 7)));
        assert_eq!(
            from_str(r#""-262144-01-01T00:00:00""#).ok(),
            Some(date::MIN.and_hms(0, 0, 0)));
        assert_eq!(
            from_str(r#""+262143-12-31T23:59:60.999999999""#).ok(),
            Some(date::MAX.and_hms_nano(23, 59, 59, 1_999_999_999)));
        assert_eq!(
            from_str(r#""+262143-12-31T23:59:60.9999999999997""#).ok(), // excess digits are ignored
            Some(date::MAX.and_hms_nano(23, 59, 59, 1_999_999_999)));

        // bad formats
        assert!(from_str(r#""""#).is_err());
        assert!(from_str(r#""2016-07-08""#).is_err());
        assert!(from_str(r#""09:10:48.090""#).is_err());
        assert!(from_str(r#""20160708T091048.090""#).is_err());
        assert!(from_str(r#""2000-00-00T00:00:00""#).is_err());
        assert!(from_str(r#""2000-02-30T00:00:00""#).is_err());
        assert!(from_str(r#""2001-02-29T00:00:00""#).is_err());
        assert!(from_str(r#""2002-02-28T24:00:00""#).is_err());
        assert!(from_str(r#""2002-02-28T23:60:00""#).is_err());
        assert!(from_str(r#""2002-02-28T23:59:61""#).is_err());
        assert!(from_str(r#""2016-07-08T09:10:48,090""#).is_err());
        assert!(from_str(r#""2016-07-08 09:10:48.090""#).is_err());
        assert!(from_str(r#""2016-007-08T09:10:48.090""#).is_err());
        assert!(from_str(r#""yyyy-mm-ddThh:mm:ss.fffffffff""#).is_err());
        assert!(from_str(r#"0"#).is_err());
        assert!(from_str(r#"20160708000000"#).is_err());
        assert!(from_str(r#"{}"#).is_err());
        assert!(from_str(r#"{"date":{"ymdf":20},"time":{"secs":0,"frac":0}}"#).is_err()); // :(
        assert!(from_str(r#"null"#).is_err());
    }

    #[test]
    fn test_serde_bincode() {
        // Bincode is relevant to test separately from JSON because
        // it is not self-describing.
        use raw::date::RawDate;
        use self::bincode::SizeLimit;
        use self::bincode::serde::{serialize, deserialize};

        let dt = RawDate::from_ymd(2016, 7, 8).and_hms_milli(9, 10, 48, 90);
        let encoded = serialize(&dt, SizeLimit::Infinite).unwrap();
        let decoded: RawDateTime = deserialize(&encoded).unwrap();
        assert_eq!(dt, decoded);
    }
}

#[cfg(test)]
mod tests {
    use super::RawDateTime;
    use Datelike;
    use duration::Duration;
    use raw::date as raw_date;
    use raw::date::RawDate;
    use std::i64;

    #[test]
    fn test_datetime_from_timestamp() {
        let from_timestamp = |secs| RawDateTime::from_timestamp_opt(secs, 0);
        let ymdhms = |y,m,d,h,n,s| RawDate::from_ymd(y,m,d).and_hms(h,n,s);
        assert_eq!(from_timestamp(-1), Some(ymdhms(1969, 12, 31, 23, 59, 59)));
        assert_eq!(from_timestamp(0), Some(ymdhms(1970, 1, 1, 0, 0, 0)));
        assert_eq!(from_timestamp(1), Some(ymdhms(1970, 1, 1, 0, 0, 1)));
        assert_eq!(from_timestamp(1_000_000_000), Some(ymdhms(2001, 9, 9, 1, 46, 40)));
        assert_eq!(from_timestamp(0x7fffffff), Some(ymdhms(2038, 1, 19, 3, 14, 7)));
        assert_eq!(from_timestamp(i64::MIN), None);
        assert_eq!(from_timestamp(i64::MAX), None);
    }

    #[test]
    fn test_datetime_add() {
        fn check((y,m,d,h,n,s): (i32,u32,u32,u32,u32,u32), rhs: Duration,
                 result: Option<(i32,u32,u32,u32,u32,u32)>) {
            let lhs = RawDate::from_ymd(y, m, d).and_hms(h, n, s);
            let sum = result.map(|(y,m,d,h,n,s)| RawDate::from_ymd(y, m, d).and_hms(h, n, s));
            assert_eq!(lhs.checked_add(rhs), sum);
            assert_eq!(lhs.checked_sub(-rhs), sum);
        };

        check((2014,5,6, 7,8,9), Duration::seconds(3600 + 60 + 1), Some((2014,5,6, 8,9,10)));
        check((2014,5,6, 7,8,9), Duration::seconds(-(3600 + 60 + 1)), Some((2014,5,6, 6,7,8)));
        check((2014,5,6, 7,8,9), Duration::seconds(86399), Some((2014,5,7, 7,8,8)));
        check((2014,5,6, 7,8,9), Duration::seconds(86400 * 10), Some((2014,5,16, 7,8,9)));
        check((2014,5,6, 7,8,9), Duration::seconds(-86400 * 10), Some((2014,4,26, 7,8,9)));
        check((2014,5,6, 7,8,9), Duration::seconds(86400 * 10), Some((2014,5,16, 7,8,9)));

        // overflow check
        // assumes that we have correct values for MAX/MIN_DAYS_FROM_YEAR_0 from `raw::date`.
        // (they are private constants, but the equivalence is tested in that module.)
        let max_days_from_year_0 = raw_date::MAX - RawDate::from_ymd(0,1,1);
        check((0,1,1, 0,0,0), max_days_from_year_0, Some((raw_date::MAX.year(),12,31, 0,0,0)));
        check((0,1,1, 0,0,0), max_days_from_year_0 + Duration::seconds(86399),
              Some((raw_date::MAX.year(),12,31, 23,59,59)));
        check((0,1,1, 0,0,0), max_days_from_year_0 + Duration::seconds(86400), None);
        check((0,1,1, 0,0,0), Duration::max_value(), None);

        let min_days_from_year_0 = raw_date::MIN - RawDate::from_ymd(0,1,1);
        check((0,1,1, 0,0,0), min_days_from_year_0, Some((raw_date::MIN.year(),1,1, 0,0,0)));
        check((0,1,1, 0,0,0), min_days_from_year_0 - Duration::seconds(1), None);
        check((0,1,1, 0,0,0), Duration::min_value(), None);
    }

    #[test]
    fn test_datetime_sub() {
        let ymdhms = |y,m,d,h,n,s| RawDate::from_ymd(y,m,d).and_hms(h,n,s);
        assert_eq!(ymdhms(2014, 5, 6, 7, 8, 9) - ymdhms(2014, 5, 6, 7, 8, 9), Duration::zero());
        assert_eq!(ymdhms(2014, 5, 6, 7, 8, 10) - ymdhms(2014, 5, 6, 7, 8, 9),
                   Duration::seconds(1));
        assert_eq!(ymdhms(2014, 5, 6, 7, 8, 9) - ymdhms(2014, 5, 6, 7, 8, 10),
                   Duration::seconds(-1));
        assert_eq!(ymdhms(2014, 5, 7, 7, 8, 9) - ymdhms(2014, 5, 6, 7, 8, 10),
                   Duration::seconds(86399));
        assert_eq!(ymdhms(2001, 9, 9, 1, 46, 39) - ymdhms(1970, 1, 1, 0, 0, 0),
                   Duration::seconds(999_999_999));
    }

    #[test]
    fn test_datetime_timestamp() {
        let to_timestamp = |y,m,d,h,n,s| RawDate::from_ymd(y,m,d).and_hms(h,n,s).timestamp();
        assert_eq!(to_timestamp(1969, 12, 31, 23, 59, 59), -1);
        assert_eq!(to_timestamp(1970, 1, 1, 0, 0, 0), 0);
        assert_eq!(to_timestamp(1970, 1, 1, 0, 0, 1), 1);
        assert_eq!(to_timestamp(2001, 9, 9, 1, 46, 40), 1_000_000_000);
        assert_eq!(to_timestamp(2038, 1, 19, 3, 14, 7), 0x7fffffff);
    }

    #[test]
    fn test_datetime_from_str() {
        // valid cases
        let valid = [
            "2015-2-18T23:16:9.15",
            "-77-02-18T23:16:09",
            "  +82701  -  05  -  6  T  15  :  9  : 60.898989898989   ",
        ];
        for &s in &valid {
            let d = match s.parse::<RawDateTime>() {
                Ok(d) => d,
                Err(e) => panic!("parsing `{}` has failed: {}", s, e)
            };
            let s_ = format!("{:?}", d);
            // `s` and `s_` may differ, but `s.parse()` and `s_.parse()` must be same
            let d_ = match s_.parse::<RawDateTime>() {
                Ok(d) => d,
                Err(e) => panic!("`{}` is parsed into `{:?}`, but reparsing that has failed: {}",
                                 s, d, e)
            };
            assert!(d == d_, "`{}` is parsed into `{:?}`, but reparsed result \
                              `{:?}` does not match", s, d, d_);
        }

        // some invalid cases
        // since `ParseErrorKind` is private, all we can do is to check if there was an error
        assert!("".parse::<RawDateTime>().is_err());
        assert!("x".parse::<RawDateTime>().is_err());
        assert!("15".parse::<RawDateTime>().is_err());
        assert!("15:8:9".parse::<RawDateTime>().is_err());
        assert!("15-8-9".parse::<RawDateTime>().is_err());
        assert!("2015-15-15T15:15:15".parse::<RawDateTime>().is_err());
        assert!("2012-12-12T12:12:12x".parse::<RawDateTime>().is_err());
        assert!("2012-123-12T12:12:12".parse::<RawDateTime>().is_err());
        assert!("+ 82701-123-12T12:12:12".parse::<RawDateTime>().is_err());
        assert!("+802701-123-12T12:12:12".parse::<RawDateTime>().is_err()); // out-of-bound
    }

    #[test]
    fn test_datetime_parse_from_str() {
        let ymdhms = |y,m,d,h,n,s| RawDate::from_ymd(y,m,d).and_hms(h,n,s);
        assert_eq!(RawDateTime::parse_from_str("2014-5-7T12:34:56+09:30", "%Y-%m-%dT%H:%M:%S%z"),
                   Ok(ymdhms(2014, 5, 7, 12, 34, 56))); // ignore offset
        assert_eq!(RawDateTime::parse_from_str("2015-W06-1 000000", "%G-W%V-%u%H%M%S"),
                   Ok(ymdhms(2015, 2, 2, 0, 0, 0)));
        assert_eq!(RawDateTime::parse_from_str("Fri, 09 Aug 2013 23:54:35 GMT",
                                                 "%a, %d %b %Y %H:%M:%S GMT"),
                   Ok(ymdhms(2013, 8, 9, 23, 54, 35)));
        assert!(RawDateTime::parse_from_str("Sat, 09 Aug 2013 23:54:35 GMT",
                                              "%a, %d %b %Y %H:%M:%S GMT").is_err());
        assert!(RawDateTime::parse_from_str("2014-5-7 12:3456", "%Y-%m-%d %H:%M:%S").is_err());
        assert!(RawDateTime::parse_from_str("12:34:56", "%H:%M:%S").is_err()); // insufficient
    }

    #[test]
    fn test_datetime_format() {
        let dt = RawDate::from_ymd(2010, 9, 8).and_hms_milli(7, 6, 54, 321);
        assert_eq!(dt.format("%c").to_string(), "Wed Sep  8 07:06:54 2010");
        assert_eq!(dt.format("%s").to_string(), "1283929614");
        assert_eq!(dt.format("%t%n%%%n%t").to_string(), "\t\n%\n\t");

        // a horror of leap second: coming near to you.
        let dt = RawDate::from_ymd(2012, 6, 30).and_hms_milli(23, 59, 59, 1_000);
        assert_eq!(dt.format("%c").to_string(), "Sat Jun 30 23:59:60 2012");
        assert_eq!(dt.format("%s").to_string(), "1341100799"); // not 1341100800, it's intentional.
    }

    #[test]
    fn test_datetime_add_sub_invariant() { // issue #37
        let base = RawDate::from_ymd(2000, 1, 1).and_hms(0, 0, 0);
        let t = -946684799990000;
        let time = base + Duration::microseconds(t);
        assert_eq!(t, (time - base).num_microseconds().unwrap());
    }
}
