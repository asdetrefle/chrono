// Copyright (c) 2016-2017, Qijie Chu.

/*!
`strftime`/`strptime`-inspired date and time formatting syntax.

## Specifiers

The following specifiers are available both to formatting and parsing.

Spec. | Example       | Description
----- | ------------- | -----------
      |               | **DATE SPECIFIERS:**
`%Y`  | `2001`        | The full proleptic Gregorian year, zero-padded to 4 digits. [1]
`%y`  | `01`          | The proleptic Gregorian year modulo 100, zero-padded to 2 digits. [2]
`%C`  | `20`          | The proleptic Gregorian year divided by 100, zero-padded to 2 digits. [2]
      |               |
`%L`  | `07`          | Month number (01--12), zero-padded to 2 digits.
`%l`  | `07`          | Month number (01--12), zero-padded to 2 digits.
`%B`  | `July`        | Full month name. Also accepts corresponding abbreviation in parsing.
`%b`  | `Jul`         | Abbreviated month name. Always 3 letters.
      |               |
`%D`  | `08`          | Day number (01--31), zero-padded to 2 digits.
`%d`  | ` 8`          | Same to `%D` but space-padded. 
      |               |
`%W`  | `Sunday`      | Full weekday name. Also accepts corresponding abbreviation in parsing.
`%w`  | `Sun`         | Abbreviated weekday name. Always 3 letters.
      |               |
`%J`  | `189`         | Day of the year (001--366), zero-padded to 3 digits.
`%j`  | `189`         | Same to `%J` but space-padded. 
      |               |
`%U`  | `07/08/01`    | Month-day-year format. Same to `%m/%d/%y`.
`%E`  | `07/08/01`    | Day-Month-year format. Same to `%m/%d/%y`.
`%F`  | `2001-07-08`  | Year-month-day format (ISO 8601). Same to `%Y-%m-%d`.
`%v`  | ` 8-Jul-2001` | Day-month-year format. Same to `%e-%b-%Y`.
      |               |
      |               | **TIME SPECIFIERS:**
`%H`  | `00`          | Hour number (00--23), zero-padded to 2 digits.
`%h`  | ` 0`          | Same to `%H` but space-padded. Same to `%_H`.
`%I`  | `12`          | Hour number in 12-hour clocks (01--12), zero-padded to 2 digits.
`%i`  | `12`          | Same to `%I` but space-padded. Same to `%_I`.
      |               |
`%P`  | `AM`          | `AM` or `PM` in 12-hour clocks.
`%p`  | `am`          | `am` or `pm` in 12-hour clocks.
      |               |
`%M`  | `34`          | Minute number (00--59), zero-padded to 2 digits.
`%S`  | `60`          | Second number (00--60), zero-padded to 2 digits. [5]
`%f`  | `.026490`     | Similar to `.%f` but left-aligned. [8]
`%3`  | `.026`        | Similar to `.%f` but left-aligned but fixed to a length of 3. [8]
`%6`  | `.026490`     | Similar to `.%f` but left-aligned but fixed to a length of 6. [8]
`%9`  | `.026490000`  | Similar to `.%f` but left-aligned but fixed to a length of 9. [8]
      |               |
`%R`  | `00:34`       | Hour-minute format. Same to `%H:%M`.
`%T`  | `00:34:60`    | Hour-minute-second format. Same to `%H:%M:%S`.
'%t'  | `32400.123456`| Hour-minute-second format. Same to `%H:%M:%S`.  
      |               | **TIME ZONE SPECIFIERS:**
`%Z`  | `+09:30`      | Same to `%z` but with a colon.
`%z`  | `+0930`       | Offset from the local time to UTC (with UTC being `+0000`).
      |               |
      |               | **DATE & TIME SPECIFIERS:**
`%+`  | `2001-07-08T00:34:60.026490+09:30` | ISO 8601 / RFC 3339 date & time format. [6]
      |               |
`%s`  | `994518299`   | UNIX timestamp, the number of seconds since 1970-01-01 00:00 UTC. [7]
      |               |
      |               | **SPECIAL SPECIFIERS:**
`%t`  |               | Literal tab (`\t`).
`%n`  |               | Literal newline (`\n`).
`%%`  |               | Literal percent sign.

It is possible to override the default padding behavior of numeric specifiers `%?`.
This is not allowed for other specifiers and will result in the `BAD_FORMAT` error.

Notes:

1. `%Y`:
   Negative years are allowed in formatting but not in parsing.

2. `%C`, `%y`:
   This is floor division, so 100 BCE (year number -99) will print `-1` and `99` respectively.

3. `%U`:
   Week 1 starts with the first Sunday in that year.
   It is possible to have week 0 for days before the first Sunday.

4. `%G`, `%g`, `%V`:
   Week 1 is the first week with at least 4 days in that year.
   Week 0 does not exist, so this should be used with `%G` or `%g`.

5. `%S`:
   It accounts for leap seconds, so `60` is possible.

6. `%+`:
   Same to `%Y-%m-%dT%H:%M:%S%.f%:z`,
   i.e. 0, 3, 6 or 9 fractional digits for seconds and colons in the time zone offset.

   The typical `strftime` implementations have
   different (and locale-dependent) formats for this specifier.
   While Chrono's format for `%+` is far more stable,
   it is best to avoid this specifier if you want to control the exact output.

7. `%s`:
   This is not padded and can be negative.
   For the purpose of Chrono, it only accounts for non-leap seconds
   so it slightly differs from ISO C `strftime` behavior.

8. `%f`, `%.f`, `%.3f`, `%.6f`, `%.9f`:

   The default `%f` is right-aligned and always zero-padded to 9 digits
   for the compatibility with glibc and others,
   so it always counts the number of nanoseconds since the last whole second.
   E.g. 7ms after the last second will print `007000000`,
   and parsing `7000000` will yield the same.

   The variant `%.f` is left-aligned and print 0, 3, 6 or 9 fractional digits
   according to the precision.
   E.g. 70ms after the last second under `%.f` will print `.070` (note: not `.07`),
   and parsing `.07`, `.070000` etc. will yield the same.
   Note that they can print or read nothing if the fractional part is zero or
   the next character is not `.`.

   The variant `%.3f`, `%.6f` and `%.9f` are left-aligned and print 3, 6 or 9 fractional digits
   according to the number preceding `f`.
   E.g. 70ms after the last second under `%.3f` will print `.070` (note: not `.07`),
   and parsing `.07`, `.070000` etc. will yield the same.
   Note that they can read nothing if the fractional part is zero or
   the next character is not `.` however will print with the specified length.

*/
#![allow(dead_code)]
#![allow(unused_must_use)]
use std::fmt::{Write, Error};
use regex::{Regex, FindCaptures};
use prelude::*;
use raw::prelude::*;
use super::prelude::*;
use offset::TimeZone;

#[derive(Clone, Debug)]
pub struct FormatSpecifier<'a> {
    specifier: &'a str,
    _regex: Regex,
}

pub struct FormatSpecifierItem<'a> {
    specifier: &'a char,
    delimiter: &'a char,
}

impl<'a> FormatSpecifier<'a> {
    pub fn new(s: &'a str) -> FormatSpecifier<'a> {
        FormatSpecifier { specifier: s, _regex: Regex::new(PATTERN).unwrap() }
    }
    
    pub fn new_<'b>(s: &'a str, fmt_str: &'b str) -> FormatSpecifier<'a> {
        FormatSpecifier { specifier: s, _regex: Regex::new(fmt_str).unwrap() }
    }

    pub fn captures_iter(&'a self) -> FindCaptures<'a, 'a> {
        self._regex.captures_iter(self.specifier)
    }
}

// Tries to format given arguments with given formatting items.
pub fn format<'a>(date: Option<&RawDate>, time: Option<&RawTime>, off: Option<&TimeZone>, 
                     fmt: &'a str) -> String {
    let fmt_spe = FormatSpecifier::new(fmt);

    let mut r = String::new();
    for item in fmt_spe.captures_iter() {
        let (s, delim) = (item.at(1).unwrap(), item.at(2).unwrap_or(""));
        let _ = match s {
            // YEAR FORMATTING
            "Y" => date.map(|d| write!(&mut r, "{}", d.year())).unwrap(),
            "y" => date.map(|d| write!(&mut r, "{}", div_floor(d.year(), 100))).unwrap(),

            // MONTH FORMATTING
            "L" => date.map(|d| write!(&mut r, "{:02}", d.month())).unwrap(),
            "l" => date.map(|d| write!(&mut r, "{}", d.month())).unwrap(),
            "B" => date.map(|d| write!(&mut r, "{}", LONG_MONTHS[d.month() as usize])).unwrap(),
            "b" => date.map(|d| write!(&mut r, "{}", SHORT_MONTHS[d.month() as usize])).unwrap(),

            // DAY FORMATTING
            "D" => date.map(|d| write!(&mut r, "{:02}", d.day())).unwrap(),
            "d" => date.map(|d| write!(&mut r, "{}", d.day())).unwrap(),

            // TIME FORMATTING
            "H" => time.map(|t| write!(&mut r, "{:02}", t.hour())).unwrap(),
            "h" => time.map(|t| write!(&mut r, "{}", t.hour())).unwrap(),
            "I" => time.map(|t| write!(&mut r, "{:02}", (t.hour() - 1) % 12 + 1)).unwrap(),
            "i" => time.map(|t| write!(&mut r, "{}", (t.hour() - 1) % 12 + 1)).unwrap(),
            "M" => time.map(|t| write!(&mut r, "{:02}", t.minute())).unwrap(),
            "S" => time.map(|t| write!(&mut r, "{:02}", t.second())).unwrap(),
            "t" => time.map(|t| write!(&mut r, "{:05}.{:0<6}", t.timestamp() as i64, (t.nanosecond() as f64 /
                                      1000.) as i64)).unwrap(),
            "P" => time.map(|t| write!(&mut r, "{}", if t.hour()>12 {"PM"} else {"AM"})).unwrap(),
            "p" => time.map(|t| write!(&mut r, "{}", if t.hour()>12 {"pm"} else {"am"})).unwrap(),
            "f" => time.map(|t| write!(&mut r, "{:0<6}", t.nanosecond() / 1_000)).unwrap(),
            "3" => time.map(|t| write!(&mut r, "{:0<3}", t.nanosecond() / 1_000_000)).unwrap(),
            "6" => time.map(|t| write!(&mut r, "{:0<6}", t.nanosecond() / 1_000)).unwrap(),
            "9" => time.map(|t| write!(&mut r, "{:0<9}", t.nanosecond())).unwrap(),
            
            // TIMEZONE FORMAT
            "Z" => off.map(|o| write!(&mut r, "{}", o)).unwrap(),
            "z" => off.map(|o| write!(&mut r, "{:?}", o)).unwrap(),
            _   => Err(Error),
            //_  => panic!("Invalid Datetime Formatting Specifier! Check for Docs.")
        };
        write!(&mut r, "{}", delim);
    }
    r
}

#[cfg(test)]
#[test]
fn test_strftime_items() {
    fn parse_and_collect<'a>(s: &'a str) -> Vec<Item<'a>> {
        // map any error into `[Item::Error]`. useful for easy testing.
        let items = StrftimeItems::new(s);
        let items = items.map(|spec| if spec == Item::Error {None} else {Some(spec)});
        items.collect::<Option<Vec<_>>>().unwrap_or(vec![Item::Error])
    }

    assert_eq!(parse_and_collect(""), []);
    assert_eq!(parse_and_collect(" \t\n\r "), [sp!(" \t\n\r ")]);
    assert_eq!(parse_and_collect("hello?"), [lit!("hello?")]);
    assert_eq!(parse_and_collect("a  b\t\nc"), [lit!("a"), sp!("  "), lit!("b"), sp!("\t\n"),
                                                lit!("c")]);
    assert_eq!(parse_and_collect("100%%"), [lit!("100"), lit!("%")]);
    assert_eq!(parse_and_collect("100%% ok"), [lit!("100"), lit!("%"), sp!(" "), lit!("ok")]);
    assert_eq!(parse_and_collect("%%PDF-1.0"), [lit!("%"), lit!("PDF-1.0")]);
    assert_eq!(parse_and_collect("%Y-%m-%d"), [num0!(Year), lit!("-"), num0!(Month), lit!("-"),
                                               num0!(Day)]);
    assert_eq!(parse_and_collect("[%F]"), parse_and_collect("[%Y-%m-%d]"));
    assert_eq!(parse_and_collect("%m %d"), [num0!(Month), sp!(" "), num0!(Day)]);
    assert_eq!(parse_and_collect("%"), [Item::Error]);
    assert_eq!(parse_and_collect("%%"), [lit!("%")]);
    assert_eq!(parse_and_collect("%%%"), [Item::Error]);
    assert_eq!(parse_and_collect("%%%%"), [lit!("%"), lit!("%")]);
    assert_eq!(parse_and_collect("foo%?"), [Item::Error]);
    assert_eq!(parse_and_collect("bar%42"), [Item::Error]);
    assert_eq!(parse_and_collect("quux% +"), [Item::Error]);
    assert_eq!(parse_and_collect("%.Z"), [Item::Error]);
    assert_eq!(parse_and_collect("%:Z"), [Item::Error]);
    assert_eq!(parse_and_collect("%-Z"), [Item::Error]);
    assert_eq!(parse_and_collect("%0Z"), [Item::Error]);
    assert_eq!(parse_and_collect("%_Z"), [Item::Error]);
    assert_eq!(parse_and_collect("%.j"), [Item::Error]);
    assert_eq!(parse_and_collect("%:j"), [Item::Error]);
    assert_eq!(parse_and_collect("%-j"), [num!(Ordinal)]);
    assert_eq!(parse_and_collect("%0j"), [num0!(Ordinal)]);
    assert_eq!(parse_and_collect("%_j"), [nums!(Ordinal)]);
    assert_eq!(parse_and_collect("%.e"), [Item::Error]);
    assert_eq!(parse_and_collect("%:e"), [Item::Error]);
    assert_eq!(parse_and_collect("%-e"), [num!(Day)]);
    assert_eq!(parse_and_collect("%0e"), [num0!(Day)]);
    assert_eq!(parse_and_collect("%_e"), [nums!(Day)]);
}

#[cfg(test)]
#[test]
fn test_strftime_docs() {
    use {FixedOffset, TimeZone};

    let dt = FixedOffset::east(34200).ymd(2001, 7, 8).and_hms_nano(0, 34, 59, 1_026_490_000);

    // date specifiers
    assert_eq!(dt.format("%Y").to_string(), "2001");
    assert_eq!(dt.format("%C").to_string(), "20");
    assert_eq!(dt.format("%y").to_string(), "01");
    assert_eq!(dt.format("%m").to_string(), "07");
    assert_eq!(dt.format("%b").to_string(), "Jul");
    assert_eq!(dt.format("%B").to_string(), "July");
    assert_eq!(dt.format("%h").to_string(), "Jul");
    assert_eq!(dt.format("%d").to_string(), "08");
    assert_eq!(dt.format("%e").to_string(), " 8");
    assert_eq!(dt.format("%e").to_string(), dt.format("%_d").to_string());
    assert_eq!(dt.format("%a").to_string(), "Sun");
    assert_eq!(dt.format("%A").to_string(), "Sunday");
    assert_eq!(dt.format("%w").to_string(), "0");
    assert_eq!(dt.format("%u").to_string(), "7");
    assert_eq!(dt.format("%U").to_string(), "28");
    assert_eq!(dt.format("%W").to_string(), "27");
    assert_eq!(dt.format("%G").to_string(), "2001");
    assert_eq!(dt.format("%g").to_string(), "01");
    assert_eq!(dt.format("%V").to_string(), "27");
    assert_eq!(dt.format("%j").to_string(), "189");
    assert_eq!(dt.format("%D").to_string(), "07/08/01");
    assert_eq!(dt.format("%x").to_string(), "07/08/01");
    assert_eq!(dt.format("%F").to_string(), "2001-07-08");
    assert_eq!(dt.format("%v").to_string(), " 8-Jul-2001");

    // time specifiers
    assert_eq!(dt.format("%H").to_string(), "00");
    assert_eq!(dt.format("%k").to_string(), " 0");
    assert_eq!(dt.format("%k").to_string(), dt.format("%_H").to_string());
    assert_eq!(dt.format("%I").to_string(), "12");
    assert_eq!(dt.format("%l").to_string(), "12");
    assert_eq!(dt.format("%l").to_string(), dt.format("%_I").to_string());
    assert_eq!(dt.format("%P").to_string(), "am");
    assert_eq!(dt.format("%p").to_string(), "AM");
    assert_eq!(dt.format("%M").to_string(), "34");
    assert_eq!(dt.format("%S").to_string(), "60");
    assert_eq!(dt.format("%f").to_string(), "026490000");
    assert_eq!(dt.format("%.f").to_string(), ".026490");
    assert_eq!(dt.format("%.3f").to_string(), ".026");
    assert_eq!(dt.format("%.6f").to_string(), ".026490");
    assert_eq!(dt.format("%.9f").to_string(), ".026490000");
    assert_eq!(dt.format("%R").to_string(), "00:34");
    assert_eq!(dt.format("%T").to_string(), "00:34:60");
    assert_eq!(dt.format("%X").to_string(), "00:34:60");
    assert_eq!(dt.format("%r").to_string(), "12:34:60 AM");

    // time zone specifiers
    //assert_eq!(dt.format("%Z").to_string(), "ACST");
    assert_eq!(dt.format("%z").to_string(), "+0930");
    assert_eq!(dt.format("%:z").to_string(), "+09:30");

    // date & time specifiers
    assert_eq!(dt.format("%c").to_string(), "Sun Jul  8 00:34:60 2001");
    assert_eq!(dt.format("%+").to_string(), "2001-07-08T00:34:60.026490+09:30");
    assert_eq!(dt.format("%s").to_string(), "994518299");

    // special specifiers
    assert_eq!(dt.format("%t").to_string(), "\t");
    assert_eq!(dt.format("%n").to_string(), "\n");
    assert_eq!(dt.format("%%").to_string(), "%");
}
