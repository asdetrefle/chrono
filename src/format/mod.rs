// Copyright (c) 2016-2017, Qijie Chu.

// Formatting utilities for date and time.
pub mod strftime;
pub mod prelude {
    pub use super::{PATTERN, SHORT_MONTHS, LONG_MONTHS, SHORT_WEEKDAYS, LONG_WEEKDAYS};
}

pub static PATTERN: &'static str = r"%([:alnum:]{1})([-:\.T/\s]?|\z)";

pub static SHORT_MONTHS: [&'static str; 12] =
    ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
pub static LONG_MONTHS: [&'static str; 12] =
    ["January", "February", "March", "April", "May", "June", 
        "July", "August", "September", "October", "November", "December"];
pub static SHORT_WEEKDAYS: [&'static str; 7] =
    ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
pub static LONG_WEEKDAYS: [&'static str; 7] =
    ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];


/*
// An error from the `parse` function.
#[derive(Debug, Clone, PartialEq, Copy)]
pub struct ParseError(ParseErrorKind);

#[derive(Debug, Clone, PartialEq, Copy)]
enum ParseErrorKind {
    // Given field is out of permitted range.
    OutOfRange,

    // There is no possible date and time value with given set of fields.
    //
    // This does not include the out-of-range conditions, which are trivially invalid.
    // It includes the case that there are one or more fields that are inconsistent to each other.
    Impossible,

    // Given set of fields is not enough to make a requested date and time value.
    //
    // Note that there *may* be a case that given fields constrain the possible values so much
    // that there is a unique possible value. Chrono only tries to be correct for
    // most useful sets of fields however, as such constraint solving can be expensive.
    NotEnough,

    // The input string has some invalid character sequence for given formatting items.
    Invalid,

    // The input string has been prematurely ended.
    TooShort,

    // All formatting items have been read but there is a remaining input.
    TooLong,

    // There was an error on the formatting string, or there were non-supported formating items.
    BadFormat,
}

// Same to `Result<T, ParseError>`.
pub type ParseResult<T> = Result<T, ParseError>;

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.description().fmt(f)
    }
}

impl Error for ParseError {
    fn description(&self) -> &str {
        match self.0 {
            ParseErrorKind::OutOfRange => "input is out of range",
            ParseErrorKind::Impossible => "no possible date and time matching input",
            ParseErrorKind::NotEnough => "input is not enough for unique date and time",
            ParseErrorKind::Invalid => "input contains invalid characters",
            ParseErrorKind::TooShort => "premature end of input",
            ParseErrorKind::TooLong => "trailing input",
            ParseErrorKind::BadFormat => "bad or unsupported format string",
        }
    }
}

// to be used in this module and submodules
const OUT_OF_RANGE: ParseError = ParseError(ParseErrorKind::OutOfRange);
const IMPOSSIBLE:   ParseError = ParseError(ParseErrorKind::Impossible);
const NOT_ENOUGH:   ParseError = ParseError(ParseErrorKind::NotEnough);
const INVALID:      ParseError = ParseError(ParseErrorKind::Invalid);
const TOO_SHORT:    ParseError = ParseError(ParseErrorKind::TooShort);
const TOO_LONG:     ParseError = ParseError(ParseErrorKind::TooLong);
const BAD_FORMAT:   ParseError = ParseError(ParseErrorKind::BadFormat);
*/


