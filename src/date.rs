// Copyright (c) 2016-2017, Qijie Chu.

// ISO 8601 calendar date with time zone.
//
// This type should be considered ambiguous at best,
// due to the inherent lack of precision required for the time zone resolution.
// There are some guarantees on the usage of `Date`:
//
// - If properly constructed via `TimeZone::ymd` and others without an error,
//   the corresponding local date should exist for at least a moment.
//   (It may still have a gap from the offset changes.)
//
// - The `TimeZone` is free to assign *any* `Offset` to the local date,
//   as long as that offset did occur in given day.
//   For example, if `2015-03-08T01:59-08:00` is followed by `2015-03-08T03:00-07:00`,
//   it may produce either `2015-03-08-08:00` or `2015-03-08-07:00`
//   but *not* `2015-03-08+00:00` and others.
//
// - Once constructed as a full `DateTime`,
//   `DateTime::date` and other associated methods should return those for the original `Date`.
//   For example, if `dt = tz.ymd(y,m,d).hms(h,n,s)` were valid, `dt.date() == tz.ymd(y,m,d)`.
//
// - The date is timezone-agnostic up to one day (i.e. practically always),
//   so the local date and UTC date should be equal for most cases
//   even though the raw calculation between `rawDate` and `Duration` may not.

use std::{fmt, hash};
use std::cmp::Ordering;
use std::ops::{Add, Sub};

use prelude::*;
use raw::prelude::*;
use raw::rawdate::MIN as RAW_MIN;
use raw::rawdate::MAX as RAW_MAX;
use offset::{TimeZone, UTC, Offset};
use datetime::DateTime;

#[derive(Clone, Copy)]
pub struct Date {
    date: RawDate,
    tz: TimeZone,
}

impl PartialEq for Date {
    fn eq(&self, other: &Date) -> bool { self.date == other.date }
}

impl PartialOrd for Date {
    fn partial_cmp(&self, other: &Date) -> Option<Ordering> {
        self.date.partial_cmp(&other.date)
    }
}

impl Eq for Date {}

impl Ord for Date {
    fn cmp(&self, other: &Date) -> Ordering { self.date.cmp(&other.date) }
}

// The minimum possible `Date`.
pub const MIN: Date = Date { date: RAW_MIN, tz: TimeZone::UTC(UTC) };
// The maximum possible `Date`.
pub const MAX: Date = Date { date: RAW_MAX, tz: TimeZone::UTC(UTC) };

impl Date {
    // Makes a new `Date` with given *UTC* date and offset.
    // The local date should be constructed via the `TimeZone` trait.
    #[inline]
    pub fn new(date: RawDate, tz: TimeZone) -> Option<Date> {
        Some(Date { date: date, tz: tz })
    }

    // Makes a new `DateTime` from the current date and given `rawTime`.
    // The offset in the current date is preserved.
    //
    // Panics on invalid datetime.
    #[inline]
    pub fn and_time(&self, time: RawTime) -> Option<DateTime> {
        self.local().and_time(time).and_then(|datetime| DateTime::new(datetime, self.tz))
    }

    // Makes a new `Date` for the next date.
    //
    // Returns `None` when `self` is the last representable date.
    #[inline]
    pub fn next(&self) -> Option<Date> {
        self.date.next().and_then(|date| Date::new(date, self.tz))
    }

    // Makes a new `Date` for the prior date.
    //
    // Returns `None` when `self` is the first representable date.
    #[inline]
    pub fn prev(&self) -> Option<Date> {
        self.date.prev().and_then(|date| Date::new(date, self.tz))
    }

    // Retrieves an associated offset from UTC.
    #[inline]
    pub fn offset(&self) -> Duration {
        self.tz.minus_utc()
    }

    // Retrieves an associated time zone.
    #[inline]
    pub fn timezone(&self) -> TimeZone {
        self.tz
    }

    // Changes the associated time zone.
    // This does not change the actual `Date` (but will change the string representation).
    #[inline]
    pub fn to_timezone(&self, tz: &TimeZone) -> Option<Date> {
        Some(Date { tz: *tz, ..*self })
    }

    // Adds given `Duration` to the current date.
    //
    // Returns `None` when it will result in overflow.
    #[inline]
    pub fn checked_add(self, rhs: Duration) -> Option<Date> {
        let date = self.date.checked_add(rhs).expect("Duration overflow");
        Some(Date { date: date, ..self })
    }

    // Subtracts given `Duration` from the current date.
    //
    // Returns `None` when it will result in overflow.
    #[inline]
    pub fn checked_sub(self, rhs: Duration) -> Option<Date> {
        let date = self.date.checked_sub(rhs).expect("Duration overflow");
        Some(Date { date: date, ..self })
    }

    // Returns a view to the raw UTC date.
    #[inline]
    pub fn into_utc(&self) -> RawDate {
        self.date
    }

    // Returns a view to the raw local date.
    #[inline]
    pub fn local(&self) -> RawDate {
        self.date
    }
    
    // Formats the date with the specified format string.
    // See the [`format::strftime` module](../format/strftime/index.html)
    // on the supported escape sequences.
    #[inline]
    pub fn format<'a>(&self, fmt: &'a str) -> String {
        format(Some(&self.local()), None, Some(&self.tz), fmt)
    }
}

// Maps the local date to other date with given conversion function.
fn map_local<F>(d: &Date, mut f: F) -> Option<Date>
        where F: FnMut(RawDate) -> Option<RawDate> {
    f(d.local()).and_then(|date| Date::new(date, d.tz))
}

/*
impl<Tz: TimeZone> Date where Tz::Offset: fmt::Display {
    // Formats the date with the specified formatting items.
    #[inline]
    pub fn format_with_items<'a, I>(&self, items: I) -> DelayedFormat<I>
            where I: Iterator<Item=Item<'a>> + Clone {
        DelayedFormat::new_with_offset(Some(self.local()), None, &self.offset, items)
    }

    // Formats the date with the specified format string.
    // See the [`format::strftime` module](../format/strftime/index.html)
    // on the supported escape sequences.
    #[inline]
    pub fn format<'a>(&self, fmt: &'a str) -> DelayedFormat<StrftimeItems<'a>> {
        self.format_with_items(StrftimeItems::new(fmt))
    }
}
*/

impl Datelike for Date {
    #[inline] 
    fn year(&self) -> i32 {
        self.local().year()
    }

    #[inline]
    fn month(&self) -> u32 {
        self.local().month()
    }

    #[inline]
    fn day(&self) -> u32 {
        self.local().day()
    }

    #[inline]
    fn ordinal(&self) -> u32 {
        self.local().ordinal()
    }

    #[inline]
    fn weekday(&self) -> Weekday {
        self.local().weekday()
    }

    #[inline]
    fn isoweekdate(&self) -> (i32, u32, Weekday) {
        self.local().isoweekdate()
    }

    #[inline]
    fn with_year(&self, year: i32) -> Option<Date> {
        map_local(self, |date| date.with_year(year))
    }

    #[inline]
    fn with_month(&self, month: u32) -> Option<Date> {
        map_local(self, |date| date.with_month(month))
    }

    #[inline]
    fn with_day(&self, day: u32) -> Option<Date> {
        map_local(self, |date| date.with_day(day))
    }

    #[inline]
    fn with_ordinal(&self, ordinal: u32) -> Option<Date> {
        map_local(self, |date| date.with_ordinal(ordinal))
    }
}

impl hash::Hash for Date {
    fn hash<H: hash::Hasher>(&self, state: &mut H) { self.date.hash(state) }
}

impl Add<Duration> for Date {
    type Output = Date;

    #[inline]
    fn add(self, rhs: Duration) -> Date {
        self.checked_add(rhs).expect("`Date + Duration` overflowed")
    }
}

impl Sub<Date> for Date {
    type Output = Duration;

    #[inline]
    fn sub(self, rhs: Date) -> Duration { self.date - rhs.date }
}

impl Sub<Duration> for Date {
    type Output = Date;

    #[inline]
    fn sub(self, rhs: Duration) -> Date {
        self.checked_sub(rhs).expect("`Date - Duration` overflowed")
    }
}

impl fmt::Debug for Date {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?} {:?}", self.local(), self.tz)
    }
}

impl fmt::Display for Date {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}{}", self.local(), self.tz)
    }
}

#[cfg(feature = "rustc-serialize")]
mod rustc_serialize {
    use super::Date;
    use offset::TimeZone;
    use rustc_serialize::{Encodable, Encoder, Decodable, Decoder};

    // TODO the current serialization format is NEVER intentionally defined.
    // in the future it is likely to be redefined to more sane and reasonable format.

    impl<Tz: TimeZone> Encodable for Date where Tz::Offset: Encodable {
        fn encode<S: Encoder>(&self, s: &mut S) -> Result<(), S::Error> {
            s.emit_struct("Date", 2, |s| {
                try!(s.emit_struct_field("date", 0, |s| self.date.encode(s)));
                try!(s.emit_struct_field("offset", 1, |s| self.offset.encode(s)));
                Ok(())
            })
        }
    }

    impl<Tz: TimeZone> Decodable for Date where Tz::Offset: Decodable {
        fn decode<D: Decoder>(d: &mut D) -> Result<Date, D::Error> {
            d.read_struct("Date", 2, |d| {
                let date = try!(d.read_struct_field("date", 0, Decodable::decode));
                let offset = try!(d.read_struct_field("offset", 1, Decodable::decode));
                Ok(Date::from_utc(date, offset))
            })
        }
    }

    #[test]
    fn test_encodable() {
        use offset::utc::UTC;
        use rustc_serialize::json::encode;

        assert_eq!(encode(&UTC.ymd(2014, 7, 24)).ok(),
                   Some(r#"{"date":{"ymdf":16501977},"offset":{}}"#.into()));
    }

    #[test]
    fn test_decodable() {
        use offset::utc::UTC;
        use rustc_serialize::json;

        let decode = |s: &str| json::decode::<Date<UTC>>(s);

        assert_eq!(decode(r#"{"date":{"ymdf":16501977},"offset":{}}"#).ok(),
                   Some(UTC.ymd(2014, 7, 24)));

        assert!(decode(r#"{"date":{"ymdf":0},"offset":{}}"#).is_err());
    }
}

#[cfg(test)]
mod tests {
    use std::fmt;

    use Datelike;
    use duration::Duration;
    use raw::date::rawDate;
    use raw::datetime::rawDateTime;
    use offset::{TimeZone, Offset, LocalResult};
    use offset::local::Local;

    #[derive(Copy, Clone, PartialEq, Eq)]
    struct UTC1y; // same to UTC but with an offset of 365 days

    #[derive(Copy, Clone, PartialEq, Eq)]
    struct OneYear;

    impl TimeZone for UTC1y {
        type Offset = OneYear;

        fn from_offset(_offset: &OneYear) -> UTC1y { UTC1y }

        fn offset_from_local_date(&self, _local: &rawDate) -> LocalResult<OneYear> {
            LocalResult::Single(OneYear)
        }
        fn offset_from_local_datetime(&self, _local: &rawDateTime) -> LocalResult<OneYear> {
            LocalResult::Single(OneYear)
        }

        fn offset_from_utc_date(&self, _utc: &rawDate) -> OneYear { OneYear }
        fn offset_from_utc_datetime(&self, _utc: &rawDateTime) -> OneYear { OneYear }
    }

    impl Offset for OneYear {
        fn local_minus_utc(&self) -> Duration { Duration::days(365) }
    }

    impl fmt::Debug for OneYear {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "+8760:00") }
    }

    #[test]
    fn test_date_weird_offset() {
        assert_eq!(format!("{:?}", UTC1y.ymd(2012, 2, 29)),
                   "2012-02-29+8760:00".to_string());
        assert_eq!(format!("{:?}", UTC1y.ymd(2012, 2, 29).and_hms(5, 6, 7)),
                   "2012-02-29T05:06:07+8760:00".to_string());
        assert_eq!(format!("{:?}", UTC1y.ymd(2012, 3, 4)),
                   "2012-03-04+8760:00".to_string());
        assert_eq!(format!("{:?}", UTC1y.ymd(2012, 3, 4).and_hms(5, 6, 7)),
                   "2012-03-04T05:06:07+8760:00".to_string());
    }

    #[test]
    fn test_local_date_sanity_check() { // issue #27
        assert_eq!(Local.ymd(2999, 12, 28).day(), 28);
    }
}

