// Copyright (c) 2016-2017, Qijie Chu.

/*!
 * The time zone, which calculates offsets from the local time to UTC.
 *
 * There are three operations provided by the `TimeZone` trait:
 *
 * 1. Converting the local `rawDateTime` to `DateTime<Tz>`
 * 2. Converting the UTC `rawDateTime` to `DateTime<Tz>`
 * 3. Converting `DateTime<Tz>` to the local `rawDateTime`
 *
 * 1 is used for constructors. 2 is used for the `with_timezone` method of date and time types.
 * 3 is used for other methods, e.g. `year()` or `format()`, and provided by an associated type
 * which implements `Offset` (which then passed to `TimeZone` for actual implementations).
 * Technically speaking `TimeZone` has a total knowledge about given timescale,
 * but `Offset` is used as a cache to avoid the repeated conversion
 * and provides implementations for 1 and 3.
 * An `TimeZone` instance can be reconstructed from the corresponding `Offset` instance.
 */

use std::fmt;
use stdtime;
use prelude::*;
use raw::rawdatetime::RawDateTime;
use date::Date;
use datetime::DateTime;

#[derive(Clone, Copy)]
pub struct UTC;

#[derive(Clone, Copy)]
pub struct Local(pub i32);

#[derive(Clone, Copy)]
pub enum TimeZone {
    UTC(UTC),
    TZ(Local),
    //City(city: String, offset: i32),
}

// The time zone.
pub trait Offset: Clone {
    // An associated offset type.
    // This type is used to store the actual offset in date and time types.
    // The original `TimeZone` value can be recovered via `TimeZone::from_offset`.
    type Tz;
    
    fn now(&self) -> DateTime;

    fn now_utc(&self) -> DateTime {
        let spec = stdtime::get_time();
        RawDateTime::from_secs(spec.sec, spec.nsec as u32)
            .map(|datetime| DateTime::from_utc(datetime, TimeZone::UTC(UTC))).unwrap()
    }
    
    fn today(&self) -> Date {
        self.now().date()
    }
    
    fn minus_utc(&self) -> Duration;

    fn minus_tz(&self, other: &TimeZone) -> Duration {
        let d1 = self.minus_utc();
        let d2 = other.minus_utc();
        d1 - d2
    }
    /*
    // Parses a string with the specified format string and
    // returns a `DateTime` with the current offset.
    // See the [`format::strftime` module](../../format/strftime/index.html)
    // on the supported escape sequences.
    //
    // If the format does not include offsets, the current offset is assumed;
    // otherwise the input should have a matching UTC offset.
    //
    // See also `DateTime::parse_from_str` which gives a local `DateTime`
    // with parsed `FixedOffset`.
    fn datetime_from_str(&self, s: &str, fmt: &str) -> ParseResult<DateTime<Self>> {
        let mut parsed = Parsed::new();
        try!(parse(&mut parsed, s, StrftimeItems::new(fmt)));
        parsed.to_datetime_with_timezone(self)
    }

    // Reconstructs the time zone from the offset.
    fn from_offset(offset: &Self::Offset) -> Self;

    // Creates the offset(s) for given local `rawDate` if possible.
    fn offset_from_local_date(&self, local: &rawDate) -> LocalResult<Self::Offset>;

    // Creates the offset(s) for given local `rawDateTime` if possible.
    fn offset_from_local_datetime(&self, local: &rawDateTime) -> LocalResult<Self::Offset>;

    // Converts the local `rawDate` to the timezone-aware `Date` if possible.
    fn from_local_date(&self, local: &rawDate) -> LocalResult<Date<Self>> {
        self.offset_from_local_date(local).map(|offset| {
            Date::from_utc(*local - offset.local_minus_utc(), offset)
        })
    }

    // Converts the local `rawDateTime` to the timezone-aware `DateTime` if possible.
    fn from_local_datetime(&self, local: &rawDateTime) -> LocalResult<DateTime<Self>> {
        self.offset_from_local_datetime(local).map(|offset| {
            let utc = add_with_leapsecond(local, &-offset.local_minus_utc());
            DateTime::from_utc(utc, offset)
        })
    }

    // Creates the offset for given UTC `rawDate`. This cannot fail.
    fn offset_from_utc_date(&self, utc: &rawDate) -> Self::Offset;

    // Creates the offset for given UTC `rawDateTime`. This cannot fail.
    fn offset_from_utc_datetime(&self, utc: &rawDateTime) -> Self::Offset;

    // Converts the UTC `rawDate` to the local time.
    // The UTC is continuous and thus this cannot fail (but can give the duplicate local time).
    fn from_utc_date(&self, utc: &rawDate) -> Date<Self> {
        Date::from_utc(utc.clone(), self.offset_from_utc_date(utc))
    }

    // Converts the UTC `rawDateTime` to the local time.
    // The UTC is continuous and thus this cannot fail (but can give the duplicate local time).
    fn from_utc_datetime(&self, utc: &rawDateTime) -> DateTime<Self> {
        DateTime::from_utc(utc.clone(), self.offset_from_utc_datetime(utc))
    }
    */
}

impl Offset for UTC {
    type Tz = UTC;

    fn now(&self) -> DateTime {
        self.now_utc()
    }    

    fn minus_utc(&self) -> Duration {
        Duration::zero()
    }
}

impl Offset for Local {
    type Tz = Local;
    
    fn now(&self) -> DateTime {
        let utc_time = self.now_utc();
        utc_time.to_timezone(&TimeZone::TZ(*self)).unwrap()
    }

    fn minus_utc(&self) -> Duration {
        let Local(offset) = *self;
        Duration::seconds(offset as i64)
    }
}

impl Offset for TimeZone {
    type Tz = TimeZone;

    fn now(&self) -> DateTime {
        match *self {
            TimeZone::UTC(ref v) => v.now(),
            TimeZone::TZ(ref v) => v.now()
        }
    }

    fn minus_utc(&self) -> Duration {
        match *self {
            TimeZone::UTC(ref v) => v.minus_utc(),
            TimeZone::TZ(ref v) => v.minus_utc()
        }
    }
}

impl fmt::Debug for UTC {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "UTC") }
}

impl fmt::Display for UTC {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "+00:00") }
}

impl fmt::Debug for Local {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let offset = self.minus_utc().num_seconds();
        let (sign, offset) = if offset < 0 {('-', -offset)} else {('+', offset)};
        let (mins, sec) = div_mod_floor(offset, 60);
        let (hour, min) = div_mod_floor(mins, 60);
        if min == 0 && sec ==0 {
            write!(f, "UTC{}{}", sign, hour)
        } else if sec == 0 {
            write!(f, "UTC{}{}:{:02}", sign, hour, min)
        } else {
            write!(f, "UTC{}{}:{:02}:{:02}", sign, hour, min, sec)
        }
    }
}

impl fmt::Display for Local {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { 
        let offset = self.minus_utc().num_seconds();
        let (sign, offset) = if offset < 0 {('-', -offset)} else {('+', offset)};
        let (mins, sec) = div_mod_floor(offset, 60);
        let (hour, min) = div_mod_floor(mins, 60);
        if sec == 0 {
            write!(f, "{}{:02}:{:02}", sign, hour, min)
        } else {
            write!(f, "{}{:02}:{:02}:{:02}", sign, hour, min, sec)
        }
    }
}

impl fmt::Debug for TimeZone {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { 
        match *self {
            TimeZone::UTC(ref v) => fmt::Debug::fmt(v, f),
            TimeZone::TZ(ref v) => fmt::Debug::fmt(v, f)
        }
    }
}

impl fmt::Display for TimeZone {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { 
        match *self {
            TimeZone::UTC(ref v) => fmt::Display::fmt(v, f),
            TimeZone::TZ(ref v) => fmt::Display::fmt(v, f)
        }
    }
}
